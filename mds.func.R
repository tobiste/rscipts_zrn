mds.func=function(zrn.data, cluster.n)
{
  
  #Transponieren der Datenmatrix
  MDS_ages=c(); MDS.with.errors=c()
  for(i in 1 : max(zrn.data$sample.id) ){
    sample.i=subset(zrn.data, sample.id==i)
    if(length(sample.i$rock)>0){
      #if(sample.i$rock[1]=="sed"){
      sample.name=sample.i$sample[1]
      ages=sample.i$age2 #disc.2
      length(ages)=max
      ages=cbind(ages)
      errors=sample.i$error2 #disc.2
      length(errors)=max
      errors=cbind(errors)
      colnames(ages)=sample.name
      colnames(errors)=sample.name
      MDS_ages=cbind(MDS_ages, ages); MDS.with.errors=cbind(MDS.with.errors,errors)
      #}
    }
  }
  
  write.csv(MDS_ages, file="MDS_ages.csv", na="", row.names=FALSE)
  mdsZ2 = read.csv("MDS_ages.csv")
  write.csv(mdsZ2, file="MDS_ages_noNA.csv", na="", row.names=FALSE)
  DZ = read.distributional("MDS_ages_noNA.csv")
  #MDS.KS.DS=MDS(diss(DZ, method="KS"))
  MDS.KS.DS=MDS(DZ, k=3, classical=F)
  return(MDS.KS.DS)
}