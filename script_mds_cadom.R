rm(list = ls()) #delete workspace
library(easypackages)
packages(
  "provenance", 
  "IsoplotR",
  "ggplot2", "ggthemes", "plotly", 
  "RColorBrewer", "colorRamps", "ggsci", "ggrepel", "data.table", "reshape2", "dplyr",
  "factoextra", "cluster", "clValid", "dbscan", "readxl", "gridExtra", prompt=F)

Sys.setenv("plotly_username" = "t.stephan")
Sys.setenv("plotly_api_key" = "GzKdAiCSO6o2zifFBLQ9")

setwd("//zfs1.hrz.tu-freiberg.de/fak3geo/GEM/Tobias Stephan/Zircon_database/R")

#Import einer xls-Datei:
zrn.db1 <- read_xls(path="//zfs1.hrz.tu-freiberg.de/fak3geo/GEM/Tobias Stephan/Zircon_database/Database/Zircon_DB_UPbages.xls", 
                    sheet=1, col_names = T, trim_ws = T,
                    na= c("#ZAHL!", "NA", "#BEZUG!", "#REF!", "#DIV/0!", "#VALUE!", "b.d.", "--", "#NAME?", "#WERT!", "<0.01", "") )
zrn.db1 <- subset(zrn.db1, !is.na(zrn.db1$Sample)) #ausschliessen von Daten ohne Sample-Namen zuweisung
zrn.db2 <- read_xls(path="//zfs1.hrz.tu-freiberg.de/fak3geo/GEM/Tobias Stephan/Zircon_database/Database/Zircon_DB_UPbages2.xls", 
                    sheet=1, col_names = T, trim_ws = T,
                    na= c("#ZAHL!", "NA", "#BEZUG!", "#REF!", "#DIV/0!", "#VALUE!", "b.d.", "--", "#NAME?", "#WERT!", "<0.01", "") )
zrn.db2 <- subset(zrn.db2, !is.na(zrn.db2$Sample)) #ausschliessen von Daten ohne Sample-Namen zuweisung
zrn.db <- rbind.data.frame(zrn.db1, zrn.db2) # zusammenfassen der beiden zrn-data dateien

sample.db <- read_xls(path="//zfs1.hrz.tu-freiberg.de/fak3geo/GEM/Tobias Stephan/Zircon_database/Database/Zircon_DB_metadata.xls", 
                      sheet=1, col_names = T, trim_ws = T,
                      na= c("#ZAHL!", "NA", "#BEZUG!", "#REF!", "#DIV/0!", "#VALUE!", "b.d.", "--", "#NAME?", "#WERT!", "<0.01", "") )
sample.db <- unique(sample.db); sample.db = subset(sample.db, !is.na(sample.db$Sample)) #ausschliessen von Daten ohne Sample-Namen zuweisung


#Import einer semicolon-getrennten txt-Datei:
#zrn.db1 <- read.csv2("Cadomian_ages/DZ_database_UPb_cad.csv", blank.lines.skip = TRUE, 
#na.strings = c("#ZAHL!", "NA", "#BEZUG!", "#REF!", "#DIV/0!", "#VALUE!", "b.d.", "--", "#NAME?", "#WERT!", "<0.01", ""), 
#header = TRUE, dec = ".", sep = ";", as.is = T)
#zrn.db1 <- subset(zrn.db1, !is.na(zrn.db1$Sample))#ausschliessen von Daten ohne Sample-Namen zuweisung
#zrn.db2 <- read.csv2("Cadomian_ages/DZ_database_UPb_cad2.csv", blank.lines.skip = TRUE, 
#na.strings = c("#ZAHL!", "NA", "#BEZUG!", "#REF!", "#DIV/0!", "#VALUE!", "b.d.", "--", "#NAME?", "#WERT!", "<0.01", ""), 
#header = TRUE, dec = ".", sep = ";", as.is = T)
#zrn.db2 <- subset(zrn.db2, !is.na(zrn.db2$Sample)) #ausschliessen von Daten ohne Sample-Namen zuweisung
#zrn.db <- rbind.data.frame(zrn.db1, zrn.db2) # zusammenfassen der beiden zrn-data dateien

#sample.db <- read.csv2("Cadomian_ages/DZ_database_samples_cad.csv", blank.lines.skip = TRUE, 
#na.strings = c("#ZAHL!", "NA", "#DIV/0!", "#VALUE!", "b.d.", "--", "#REF!", "#NAME?", "#WERT!", "<0.01", ""), 
#header = TRUE, dec = ".", sep = ";", as.is = T)
#sample.db <- unique(sample.db); sample.db = subset(sample.db, !is.na(sample.db$Sample)) #ausschliessen von Daten ohne Sample-Namen zuweisung


# zusammenfassen der zrn-data mit den metadaten
zrn1 <- merge(zrn.db, sample.db, by.x = "Sample", by.y = "Sample", all.x = T)  
zrn1 <- unique(zrn1); # duplikate ausschliessen
zrn1 <- subset(zrn1, zrn1$Plot == "y") # ausschliessen von bestimmten daten  

# falls mit R berechneten Dateien arbeiten:
zrn0 <- read.csv("ages_calc.csv", sep = ";")# die unter ratio2age.R berechneten Alter
zrn0 <- unique(zrn0)


table.s1 <- unique(data.frame(zrn1$Reference.x, zrn1$Sample, zrn1$Sample_orig.x, zrn1$Rocktype, 
                              zrn1$MetamGrade, zrn1$Location, zrn1$TectUnit, zrn1$Formation, zrn1$StratiStage, zrn1$SampleAge, 
                              zrn1$Lat.WGS, zrn1$Long.WGS, zrn1$Method, zrn1$Signature2, zrn1$Prov.Sign, zrn1$Rock))
table.s1 <- table.s1[order(table.s1$zrn1.Reference.x, table.s1$zrn1.Sample, table.s1$zrn1.Sample_orig.x), ]
table.s1 <- subset(table.s1, table.s1$zrn1.SampleAge >= 400)
table.s1 <- subset(table.s1, table.s1$zrn1.SampleAge <= 700)
table.s1 <- subset(table.s1, table.s1$zrn1.Rock == "sed")
write.csv(table.s1, file = "table-s1.csv", na = "", row.names = FALSE)

a.sample <- length(unique(zrn1$Sample))
a.ref <- length(unique(zrn1$Reference.x))
a.age <- length(zrn1$PrefAge2)

# zuweisung der parameter
reference <- zrn1$Reference.x
sample <- zrn1$Sample
Pb6U8.age <- zrn1$`206Pb.238U_calc`# 206Pb/238U age
errorage68 <- zrn1$Error68_2s # Error 206Pb/238U age in 2s
Pb7Pb6.age <- zrn1$`207Pb.206Pbage_calc` # 207Pb/206Pb age
errorage76 <- zrn1$Error_76_calc_2sMa # Error 206Pb/238U age in 2s
Pb7U5.age <- zrn1$`207Pb.235Uage` # 207Pb/235U age
errorage75 <- zrn1$Error75_2s # Error 207Pb/235U age in 2s
rock <- zrn1$Rock # Rocktype
rock.type <- zrn1$Rocktype
formation <- zrn1$Formation
metam <- zrn1$MetamGrade # Metmorphic grade
location <- zrn1$Location # Sample Location
unit.abbr <- zrn1$TectU.abbr # Tectonic unit 
peaks <- zrn1$Peaks # Tectonic unit Number
signat <- zrn1$Signature # Provenance Signature
signat2 <- zrn1$Signature2 # Provenance Signature
unit <- zrn1$TectUnit
mds.group <- zrn1$MDS.Group
ref.sample <- zrn1$Prov.Sign

sample.age <- zrn1$SampleAge # min. Sample age
method <- zrn1$Method # Method of age dating (e.g. SHRIMP, LA-ICP-MS, ...)
age <- zrn1$PrefAge
error <- zrn1$Error_PrefAge
age2 <- zrn1$PrefAge2
error2 <- zrn1$Error_PrefAge2
age3 <- zrn1$PrefAge3
error3 <- zrn1$Error_PrefAge3

Pb6U8 <- zrn1$`206Pb.238U` # 206Pb/238U ratio
error68 <- zrn1$Error2_2s.perc / 100 * zrn1$`206Pb.238U` # Error 206Pb/238U ratio in 2s abs
Pb7Pb6 <- zrn1$`207Pb.206Pb` # 207Pb/206Pb ratio
error76 <- zrn1$Error4_2s.perc / 100 * zrn1$`207Pb.206Pb` # Error 206Pb/238U ratio in 2s abs
Pb7U5 <- zrn1$`207Pb.235U_calc` # 207Pb/235U ratio
error75 <- zrn1$Error3_2s.perc / 100 * zrn1$`207Pb.235U_calc` # Error 207Pb/235U ratio in 2s abs

rho <- error68 / error75


ThU <- zrn1$Th.U
discord <- (Pb6U8 - Pb7U5) / Pb7U5 * 100 # Discordance (after M. Kirsch)
concord <- Pb6U8 / Pb7Pb6 * 100 # "classical" Concordance

# Tabelle reduzieren auf relevante parameter
zrn <- data.frame(reference, sample, age, error, age2, error2, age3, error3, rock, rock.type, metam, location, formation, 
                  unit.abbr, peaks, unit, sample.age, method, ThU, Pb7U5, error75, Pb6U8, error68, Pb7Pb6, error76, ref.sample, 
                  signat, signat2, lat = zrn1$Lat.WGS, long = zrn1$Long.WGS,
                  stringsAsFactors = F)

# Reference ID zuweisen
zrn <- zrn[order(zrn$reference), ]
ref.nr <- data.frame(reference = unique(zrn$reference), ref.nr = as.character(1:length(unique(zrn$reference))))
zrn <- merge(zrn, ref.nr, all.x = T, by.x = "reference", by.y = "reference")
write.csv2(ref.nr, file = "//zfs1.hrz.tu-freiberg.de/fak3geo/GEM/Tobias Stephan/HiWi/Zircon_database/reference_nr.csv", row.names = F, na = "")

zrn <- zrn[order(zrn$unit.abbr, zrn$location, zrn$unit, -zrn$sample.age, zrn$rock, zrn$reference, zrn$sample), ]
zrn <- zrn[order(zrn$sample), ]

#Sample ID0 zuweisen
sample.id0 <- data.frame(sample = unique(zrn$sample), sample.id0 = 1:length(unique(zrn$sample)))
zrn <- merge(zrn, sample.id0, all.x = T, by.x = "sample", by.y = "sample")

#Liste mit Anzahl der konkordanten Alter je Probe
for(i in 1 : max(zrn$sample.id0) )
{
  sample.i <- subset(zrn, sample.id0 == i)
  if (i == 1){ k <- data.frame( sample.i$sample[1], length(sample.i$age) ) }
  if (i>1){k <- rbind(k, data.frame( sample.i$sample[1], length(sample.i$age) ) ) } 
}
klist <- data.frame(sample = k$sample.i.sample.1., k = k$length.sample.i.age.)
zrn <- merge(zrn, klist, by.x = "sample", by.y = "sample")



#FILTER#####################################################################################

zrn2 <- subset(zrn, zrn$rock == "sed")
# Cut off wert
#cut.off <- 700 ; zrn2 = subset(zrn2, zrn2$age3 >= cut.off)
cut.off <- 700 ; zrn2 <- subset(zrn2, zrn2$age3 <= 700 & zrn2$age3 >= 400 | zrn2$sample.age == 0 )

###########################################################################################





#Sample ID zuweisen
sample.id <- data.frame(sample = unique(zrn2$sample), sample.id = 1:length(unique(zrn2$sample)))
zrn2 <- merge(zrn2, sample.id, all.x = T, by.x = "sample", by.y = "sample")


# Check if there are samples that are not merged correctly
unique(subset(zrn2, is.na(sample.age), select = c(reference, sample)))
#zrn2 <- unique(zrn2)
zrn2 <- zrn2[order(zrn2$sample.id),]



# Maximale Länge eines Datensatzes herausfinden
max <- 0
for(i in 1:max(zrn2$sample.id)){
  sample.i <- subset(zrn2, sample.id == i)
  max2 <- length(sample.i$age)
  if(max <= max2){max <- max2}
}
max

# histogramm aller alter
ggplot(zrn, aes(age)) + geom_histogram(binwidth = 20, color="dodgerblue", size=.5) + 
  scale_x_continuous(limits= c(0, 3600), breaks = seq(from = 0, to = 3600, by = 200)) + 
  geom_vline(xintercept = c(400, 700)) + 
  ggtitle(label="Distribution of all ages", subtitle=paste("N = ", length(zrn$age)) )


a <- subset(zrn2, zrn2$age>= 400 | zrn2$age< 700)
#Liste mit Anzahl der konkordanten Alter >700 Ma je Probe
for(i in 1 : max(a$sample.id)){
  sample.i <- subset(a, sample.id == i)
  if(length(sample.i$age)>0){
    if (i == 1){k2 <- length(sample.i$age); k.name <- sample.i$sample[1]}
    if (i>1){k2 <- rbind(k2, length(sample.i$age)); k.name <- rbind(k.name, sample.i$sample[1])}
  }
}
klist2 <- data.frame("Sample" = k.name, "k400" = k2)

pdf(file = "statistics.pdf", paper = "a4r", useDingbats = FALSE, family = "sans"); par(xpd = F) #
ggplot(zrn, aes(age)) + geom_histogram(binwidth = 20) + 
  scale_x_continuous(breaks = seq(from = 0, to = 3600, by = 250)) + 
  geom_vline(xintercept = c(400, 700)) + 
  ggtitle("Distribution of all concordant ages \nN = ", length(zrn$age) )

ggplot(klist, aes(k)) + geom_histogram(binwidth = 5) + geom_vline(xintercept = c(117, median(klist$k), mean(klist$k))) + 
  scale_x_continuous(breaks = seq(from = 0, to = max, by = 20)) +
  ggtitle("Distribution of k") + 
  annotate("text", y = c(4,3,2,1), x = c(1,1,1,1), label = c("Median",paste(median(klist$k)), "Mean", round(mean(klist$k), digits = 1)))

ggplot(klist2, aes(k2)) + geom_histogram(binwidth = 5) + geom_vline(xintercept = c(30, median(klist2$k2), mean(klist2$k2))) + 
  scale_x_continuous(breaks = seq(from = 0, to = max, by = 20)) +
  ggtitle("Distribution of k (>700 Ma)") + 
  annotate("text", y = c(4,3,2,1), x = c(1,1,1,1), label = c("Median",paste(median(klist2$k2)), "Mean", round(mean(klist2$k2), digits = 1)))
dev.off()

dim(subset(klist, klist$k>= 117))[1]/dim(klist)[1] #fractional prop. of samples satisfiying the k = 117 condition
dim(subset(klist, klist$k<117))[1]/dim(klist)[1] #fractional prop. of samples not satisfiying the k = 117 condition

length(subset(zrn$age, zrn$age>= 700))/length(subset(zrn$age, zrn$age>= 400)) # fract. prop of all ages > 700 Ma
length(subset(zrn$age, zrn$age<700 & zrn$age >= 400))/length(subset(zrn$age, zrn$age>= 400)) # fract. prop of all ages < 700 Ma and >= 400 Ma, i.e. Cadomian

klist3 <- merge(klist, klist2, by.x = "sample", by.y = "Sample", all.x = T)
write.csv2(klist3, "list_of_k.csv", row.names = F)

zrn2 <- merge(zrn2, klist2, by.x = "sample", by.y = "Sample", all.x = T)



#FILTER#####################################################

zrn3 <- subset( zrn2, zrn2$k400 >= 10 ) #Ausschliessen von Datensätzen mit weniger als k age2 werten

zrn_preDev <- subset(zrn3, zrn3$sample.age >= 400 | zrn3$sample.age == 0) # only samples with depositional ages older than 400
zrn_preDev <- subset(zrn_preDev, zrn_preDev$sample.age <= 700) # Keine Proben älter als 700 Ma

#####################################################



zrn.data <- zrn_preDev


#Sample ID zuweisen
sample.id2 <- data.frame(sample = unique(zrn.data$sample), sample.id2 = 1:length(unique(zrn.data$sample)))
zrn.data <- merge(zrn.data, sample.id2, all.x = T, by.x = "sample", by.y = "sample")


# Size of Database
b.sample <- length(unique(zrn.data$sample)); b.ref <- length(unique(zrn.data$reference)); b.age <- length(zrn.data$age2)
database.size <- data.frame("n.references" = a.ref, "n.samples" = a.sample, "n.ages" = a.age); 
database.size = rbind(database.size, data.frame("n.references" = b.ref, "n.samples" = b.sample, "n.ages" = b.age)); 
row.names(database.size) = c("unfiltered", "filtered")
write.csv2(database.size, "database_size.csv")


#Transponieren der Datenmatrix
MDS_ages <- c(); MDS.with.errors <- c()
for(i in 1 : max(zrn.data$sample.id2) ){
  sample.i <- subset(zrn.data, zrn.data$sample.id2 == i)
  if(length(sample.i$rock)>0){
    #if(sample.i$rock[1] == "sed"){
    sample.name <- sample.i$sample[1]
    ages <- sample.i$age #disc.1
    length(ages) <- max
    ages <- cbind(ages)
    errors <- sample.i$error #disc.
    length(errors) <- max
    errors <- cbind(errors)
    colnames(ages) <- sample.name
    colnames(errors) <- sample.name
    MDS_ages <- cbind(MDS_ages, ages); MDS.with.errors <- cbind(MDS.with.errors,errors)
    #}
  }
}


write.csv(MDS_ages, file = "MDS_ages.csv", na = "", row.names = FALSE)
write.csv(MDS.with.errors, file = "MDS_errors.csv", na = "", row.names = FALSE)

mdsZ2 <- read.csv("MDS_ages.csv")
mdsZ2.errors <- read.csv("MDS_errors.csv")
names(mdsZ2) <- colnames(MDS_ages) #read.csv verändert strings des headers, hiermit ersetze ich die "neuen" sample_names mit den eigentlichen
names(mdsZ2.errors) <- colnames(MDS_ages)

write.csv(mdsZ2, file = "MDS_ages_noNA.csv", na = "", row.names = FALSE)
write.csv(mdsZ2.errors, file = "MDS_errors_noNA.csv", na = "", row.names = FALSE)

DZ <- read.distributional("MDS_ages_noNA.csv", "MDS_errors_noNA.csv", method = "KS") # method = "SH" for Silcombe-Hazelton


#############################################################################################################
# MDS
#############################################################################################################

dim <- 5
MDS.KS.DS <- MDS(DZ, k = dim, classical = F) #dissimilarity measure, Kolmogorov-Smirnov statistic, non-metric multidimensional scaling, k-dimensional output
#KS.diss(x, y); SH.diss(DZ, 1, 2, c.con = 0); Kuiper.diss(x, y)
#plot(MDS.KS.DS, nnlines = T)

mds.variance <- var(MDS.KS.DS$points); mds.variance[lower.tri(mds.variance)] <- NA; mds.variance

KS_matrix <- as.matrix(MDS.KS.DS$diss) # erstellen der matrix mit den K-S werten, Clusterung erfolgt mit dieser matrix

UPb <- KDEs(DZ, from = 400, to = 700, normalise = F, adaptive = T, samebandwidth = T, bw = 30)


############################################################################################################# 
#1st Clustering
#############################################################################################################

#Cluster1 <- hdbscan(MDS.KS.DS$points, minPts = 3, gen_simplified_tree = T, gen_hdbscan_tree = T) # Hierarchical DBSCAN of MDS configuration

Cluster <- hdbscan(KS_matrix, minPts = 3, gen_simplified_tree = F, gen_hdbscan_tree = F) # Hierarchical DBSCAN of Kolmogorov-Smirnov distances
Cluster

#  zuweisen der cluster parameter zu den mds results
DZages <- mdsZ2
mds <- data.frame(MDS.X = MDS.KS.DS$points[,1], MDS.Y = MDS.KS.DS$points[,2], MDS.Z = MDS.KS.DS$points[,3], 
                  MDS.4 = MDS.KS.DS$points[,4], MDS.5 = MDS.KS.DS$points[,5], 
                  sample = names(DZages), HDBSCAN.Cluster = as.character(Cluster$cluster), Prob = Cluster$membership_prob, 
                  stringsAsFactors = F, row.names = NULL) # fuer qplot

# zuweisen weiterer metadaten 
q <- unique(data.frame(sample = zrn.data$sample, Nr = zrn.data$ref.nr, Location = zrn.data$location, 
                       Unit = zrn.data$unit, Rock = zrn.data$rock.type, Tectonic.unit = zrn.data$unit.abbr, 
                       Sample.age = zrn.data$sample.age, Reference = zrn.data$reference, Rock.type = zrn.data$rock, 
                       Sample.ID = zrn.data$sample.id, ZrnProvince = zrn.data$signat2, Long = zrn.data$long, 
                       Lat = zrn.data$lat, k400Ma = zrn.data$k400, stringsAsFactors = F))
mds2 <- merge(mds, q, all.x = T, by.x = "sample", by.y = "sample")
mds2$ZrnProvince[is.na(mds2$ZrnProvince)] <- "unknown"


n <- length(mds2$sample) #- length(mds2[grep("ref", mds2$Peaks),]$sample) # anzahl der samples, exkl. der reference-samples

unique(mds2$ZrnProvince)
col.pal <- c(
  #rgb(19, 123, 94, maxColorValue = 255), #Amazonia
  rgb(19, 123, 94, maxColorValue = 255), #Amazonia-ref
  rgb(255, 182, 0, maxColorValue = 255), #ANS
  rgb(255, 182, 0, maxColorValue = 255), #ANS-ref
  rgb(37, 147, 126, maxColorValue = 255), #Avalonia
  #rgb(31, 119, 180, maxColorValue = 255), #Baltica
  rgb(31, 180, 165, maxColorValue = 255), #Baltica-Amazonia
  rgb(31, 119, 180, maxColorValue = 255), #Baltica-ref
  rgb(0, 50, 84, maxColorValue = 255), #Laurentia
  #rgb(68, 68, 68, maxColorValue = 255), #reference
  #rgb(4, 86, 55, maxColorValue = 255), #Sao Francisco-ref
  rgb(255, 127, 14, maxColorValue = 255), #SMC
  rgb(255, 127, 14, maxColorValue = 255), #SMC-ref
  rgb(127, 127, 127, maxColorValue = 255), #unknown
  rgb(214, 39, 40, maxColorValue = 255), #WAC
  rgb(86, 26, 7, maxColorValue = 255), #WAC-Amazonia
  rgb(214, 39, 40, maxColorValue = 255) #WAC-ref
)




mds2$HDBSCAN.Cluster[mds2$HDBSCAN.Cluster == 0] <- "outlier"
write.csv(mds2, "Cadom-hbscan_results.csv", dec = ".")

# plotten der HDBSCAN results 
p1 <- plot_ly(type = "scatter3d", x = mds2$MDS.X, y = mds2$MDS.Y, z = mds2$MDS.Z, mode = "markers", text = mds2$sample, 
              color = mds2$HDBSCAN.Cluster, 
              #marker = list(size = 100*mds2$Prob, sizemode = "diameter", sizemin = 2, sizeref = 3, opacity = 0.9, line = list(color = "transparent", width = 0)),
              hovertext = paste("Sample", mds2$sample,
                                "\nReference:", mds2$Reference, 
                                "\nAge:", mds2$Sample.age, "Ma",
                                #"Ma\nUnit:", mds2$Tectonic.unit,
                                "\nRock:", mds2$Rock,
                                "\nLocation:", mds2$Location,
                                "\nUnit:", mds2$Unit,
                                "\nLongitude:", round(mds2$Long, digits = 2), 
                                "Latitude:", round(mds2$Lat, digits = 2),
                                "\nNumber of zircon grains (k):", mds2$k400Ma),
              hoverinfo = "text+name"
)%>%
  layout(scene = list(xaxis = list(title = 'Dim1', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, 
                                   showbackground = TRUE, showline = TRUE, showticklabels = FALSE),
                      yaxis = list(title = 'Dim2', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, 
                                   showbackground = TRUE, showline = TRUE, showticklabels = FALSE),
                      zaxis = list(title = 'Dim5', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, 
                                   showbackground = TRUE, showline = TRUE, showticklabels = FALSE)), 
         title = paste("&nbsp;&lt;b&gt;Figure S6:&lt;/b&gt; Multi-dimensional scaling and HDBSCAN clustering of Cadomian record (minPts = ", Cluster$minPts, ")"), 
         paper_bgcolor = "rgb(230, 230, 230)"
        )
#api_create(p1, filename = "Cadom-HDBSCAN-KS-clust", fileopt = "overwrite", sharing= "private") 
p1

# 3-dimensional nMDS der bereits ermittelten zircon provinzen
p0 <- plot_ly(type = "scatter3d", x = mds2$MDS.X, y = mds2$MDS.Y, z = mds2$MDS.Z, mode = "markers", text = mds2$sample, 
              color = mds2$ZrnProvince, colors = col.pal, 
              marker = list(opacity = 1, line = list(color = "transparent", width = 0)),
              hovertext = paste("Sample", mds2$sample,
                                "\nReference:", mds2$Reference, 
                                "\nAge:", mds2$Sample.age, "Ma",
                                #"Ma\nUnit:", mds2$Tectonic.unit,
                                "\nRock:", mds2$Rock,
                                "\nLocation:", mds2$Location,
                                "\nUnit:", mds2$Unit,
                                "\nLongitude:", round(mds2$Long, digits = 2), 
                                "Latitude:", round(mds2$Lat, digits = 2),
                                "\nNumber of zircon grains (k):", mds2$k400Ma),
              hoverinfo = "text+name"
)%>%
  layout(scene = list(xaxis = list(title = 'Dim1', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, 
                                   showbackground = TRUE, showline = TRUE, showticklabels = FALSE),
                      yaxis = list(title = 'Dim2', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, 
                                   showbackground = TRUE, showline = TRUE, showticklabels = FALSE),
                      zaxis = list(title = 'Dim3', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, 
                                   showbackground = TRUE, showline = TRUE, showticklabels = FALSE)), 
         title = paste("&nbsp;&lt;b&gt;Figure S9:&lt;/b&gt; nMDS of all meta-sedimentary rocks (>400 Ma) without ages older than 700 Ma, S = ", 
                       round(MDS.KS.DS$stress/100, digits = 2), "(n = ", n, ", k >= 10)"), 
         paper_bgcolor = "rgb(230, 230, 230)")
#api_create(p0, filename = "Cadom3_ZP", fileopt = "overwrite", sharing= "private")
p0

# 3-dimensional nMDS (mit 5. Dimension der MDS results) der bereits ermittelten zircon provinzen  
p0 <- plot_ly(type = "scatter3d", x = mds2$MDS.X, y = mds2$MDS.Y, z = mds2$MDS.5, mode = "markers", text = mds2$sample, 
              color = mds2$ZrnProvince, colors = col.pal, 
              marker = list(opacity = 1, line = list(color = "transparent", width = 0)),
              hovertext = paste("Sample", mds2$sample,
                                "\nReference:", mds2$Reference, 
                                "\nAge:", mds2$Sample.age, "Ma",
                                #"Ma\nUnit:", mds2$Tectonic.unit,
                                "\nRock:", mds2$Rock,
                                "\nLocation:", mds2$Location,
                                "\nUnit:", mds2$Unit,
                                "\nLongitude:", round(mds2$Long, digits = 2), 
                                "Latitude:", round(mds2$Lat, digits = 2),
                                "\nNumber of zircon grains (k):", mds2$k400Ma),
              hoverinfo = "text+name"
)%>%
  layout(scene = list(xaxis = list(title = 'Dim1', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, 
                                   showbackground = TRUE, showline = TRUE, showticklabels = FALSE),
                      yaxis = list(title = 'Dim2', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, 
                                   showbackground = TRUE, showline = TRUE, showticklabels = FALSE),
                      zaxis = list(title = 'Dim5', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, 
                                   showbackground = TRUE, showline = TRUE, showticklabels = FALSE)), 
         title = paste("&nbsp;&lt;b&gt;Figure S9:&lt;/b&gt; nMDS of all meta-sedimentary rocks (>400 Ma) without ages older than 700 Ma, S = ", round(MDS.KS.DS$stress/100, digits = 2), "(n = ", n, ", k >= 10)"), 
         paper_bgcolor = "rgb(230, 230, 230)")
api_create(p0, filename = "Cadom5_ZP", fileopt = "overwrite", sharing= "private")






u <- ggplot(data = mds2)
pdf(file = "1st_Cluster_results-Cadom.pdf", width = 15, height = 15, useDingbats = FALSE, family = "sans"); par(xpd = F) 
u + geom_point(aes(MDS.X, MDS.Y, color = HDBSCAN.Cluster), size = 4) +
  #geom_text_repel(aes(MDS.X, MDS.Y, label = sample, color = HDBSCAN.Cluster), size = 6) +
  labs(title = "HDBSCAN", y = "DIM 2", x = "DIM 1")+ 
  scale_color_brewer(palette = "Set1")+
  theme_solarized_2(light = F)
u + geom_point(aes(MDS.X, MDS.Z, color = HDBSCAN.Cluster), size = 4) +
  #geom_text_repel(aes(MDS.X, MDS.Z, label = sample, color = HDBSCAN.Cluster), size = 6) +
  labs(title = "HDBSCAN", y = "DIM 3", x = "DIM 1")+ 
  scale_color_brewer(palette = "Set1")+
  theme_solarized_2(light = F)
u + geom_point(aes(MDS.Z, MDS.Y, color = HDBSCAN.Cluster), size = 4) +
  #geom_text_repel(aes(DIM2, DIM3, label = sample, color = HDBSCAN.Cluster), size = 6) +
  labs(title = "HDBSCAN", y = "DIM 2", x = "DIM 3")+ 
  scale_color_brewer(palette = "Set1")+
  theme_solarized_2(light = F)
dev.off()




############################################################################################################# 
# 2nd Clustering
#############################################################################################################

# which cluster has the most elements? supposed to be the bulk Africa cluster
z <- rle(sort(subset(mds2, !mds2$HDBSCAN.Cluster=="outlier")$HDBSCAN.Cluster)); # ausschließen der outlier, sortieren der matrix nach cluster-values
z <- data.frame(cluster = z$values, length=z$lengths); z <- z[order(-z$length),] 
z

big.cluster <- z[1,1] # weiteres clustern des groeßten hdbscan-clusters (bulk africa)

zrn.data2 <- merge(zrn.data, mds, by.x = "sample", by.y = "sample", all.x = T)
zrn.data2 <- subset(zrn.data2, zrn.data2$HDBSCAN.Cluster == big.cluster)


#Transponieren der Datenmatrix
MDS_ages <- c(); MDS.with.errors <- c()
for(i in 1 : max(zrn.data2$sample.id2) ){
  sample.i <- subset(zrn.data2, sample.id2 == i)
  if(length(sample.i$rock) > 0){
    #if(sample.i$rock[1] == "sed"){
    sample.name <- sample.i$sample[1]
    ages <- sample.i$age #disc.1
    length(ages) <- max
    ages <- cbind(ages)
    errors <- sample.i$error #disc.
    length(errors) <- max
    errors <- cbind(errors)
    colnames(ages) <- sample.name
    colnames(errors) <- sample.name
    MDS_ages <- cbind(MDS_ages, ages); MDS.with.errors <- cbind(MDS.with.errors,errors)
    #}
  }
}


write.csv(MDS_ages, file = "MDS_ages2.csv", na = "", row.names = FALSE)
mdsZ2 <- read.csv("MDS_ages2.csv")
names(mdsZ2) <- colnames(MDS_ages) #read.csv verändert strings des headers, hiermit ersetze ich die "neuen" sample_names mit den eigentlichen
write.csv(mdsZ2, file = "MDS_ages_noNA2.csv", na = "", row.names = FALSE)

DZ2 <- read.distributional("MDS_ages_noNA2.csv")



dim <- 3
MDS.KS.DS2 <- MDS(DZ2, k = dim, classical = F) #dissimilarity measure, Kolmogorov-Smirnov statistic, non-metric multidimensional scaling, k-dimensional output
KS_matrix2 <- as.matrix(MDS.KS.DS2$diss)

res.hc <- eclust(KS_matrix2, "hclust", k = 3, graph = FALSE, hc_method = "ward.D2", stand = T, outlier.color = "black", outlier.shape = 19) # Compute hierarchical clustering and cut into k clusters
#colnames(res.hc$data) = names(mdsZ2)

mds_2 <- data.frame("sample" = colnames(res.hc$data), "DIM1" = MDS.KS.DS2$points[,1], "DIM2" = MDS.KS.DS2$points[,2], "DIM3" = MDS.KS.DS2$points[,3], row.names = NULL)
Cluster2 <- data.frame("sample" = row.names(res.hc$silinfo$widths), "cluster" = res.hc$silinfo$widths$cluster, "neighbor" = res.hc$silinfo$widths$neighbor, "sil_width" = res.hc$silinfo$widths$sil_width)
Cluster2 <- merge(mds_2, Cluster2, by.x = "sample", by.y = "sample"); write.csv(Cluster2, "HCA_results.csv")

z <- ggplot(data = Cluster2)
UPb <- KDEs(DZ2, from = 400, to = 700, normalise = F, adaptive = T, samebandwidth = T, bw = 30)

pdf(file = "2nd_Cluster_results-Cadom.pdf", width = 15, height = 15, useDingbats = FALSE, family = "sans"); par(xpd = F) 
fviz_cluster(res.hc, show.clust.cent = F, ellipse = T, ellipse.type = "convex", xlab = "Dim1", ylab = "Dim2", pointsize = 1, labelsize = 8, ggtheme = theme_hc(bgcolor = "darkunica"), palette = "npg", main = "HCA") #
fviz_dend(res.hc, rect = T, cex = 0.5, type = "rectangle", horiz = T, lwd = .5, ggtheme = theme_hc(bgcolor = "darkunica"), palette = "npg", main = "HCA") #
fviz_dend(res.hc, rect = T, cex = 0.5, type = "phylogenic", lwd = .5, repel = T, ggtheme = theme_hc(bgcolor = "darkunica"), palette = "npg", main = "HCA") #
fviz_dend(res.hc, rect = T, cex = 0.5, type = "circular", lwd = .5, ggtheme = theme_classic(), palette = "npg", main = "HCA") #
grid.arrange(
  fviz_nbclust(KS_matrix2, hcut, method = "silhouette", verbose = F, ggtheme = theme_classic(), palette = "npg"), #optimale anzahl an clustern errechnen
  fviz_nbclust(KS_matrix2, hcut, method = "wss", verbose = F, ggtheme = theme_classic(), palette = "npg"), #optimale anzahl an clustern errechnen
  fviz_nbclust(KS_matrix2, hcut, method = "gap_stat", verbose = F, ggtheme = theme_classic(), palette = "npg"),#optimale anzahl an clustern errechnen
  nrow=3)
fviz_silhouette(res.hc, ggtheme = theme_hc(bgcolor = "darkunica"), palette = "npg") 
z + geom_point(aes(DIM2, DIM1, color = cluster, shape = cluster), size = 4) +
  geom_text_repel(aes(DIM2, DIM1, label = sample, color = cluster), size = 6) +
  labs(title = "HCA", y = "DIM 1", x = "DIM 2")+ 
  scale_color_brewer(palette = "Set1")+
  theme_solarized_2(light = F)
z + geom_point(aes(DIM3, DIM1, color = cluster, shape = cluster), size = 4) +
  geom_text_repel(aes(DIM3, DIM1, label = sample, color = cluster), size = 6) +
  labs(title = "HCA", y = "DIM 1", x = "DIM 3")+ 
  scale_color_brewer(palette = "Set1")+
  theme_solarized_2(light = F)
z + geom_point(aes(DIM2, DIM3, color = cluster, shape = cluster), size = 4) +
  geom_text_repel(aes(DIM2, DIM3, label = sample, color = cluster), size = 6) +
  labs(title = "HCA", y = "DIM 3", x = "DIM 2")+ 
  scale_color_brewer(palette = "Set1")+
  theme_solarized_2(light = F)
#aKDE
if(length(unique(zrn.data2$sample)) < 100) {summaryplot(UPb, ncol = 1)}
if(length(unique(zrn.data2$sample)) >= 100 & length(unique(zrn.data2$sample)) < 200) {summaryplot(UPb, ncol = 2)}
if(length(unique(zrn.data2$sample)) >= 200 & length(unique(zrn.data2$sample)) < 600) {summaryplot(UPb, ncol = 3)}
if(length(unique(zrn.data2$sample)) >= 600 ) {summaryplot(UPb, ncol = 4)}

ggplot(Cluster2, aes(sil_width)) + geom_histogram(binwidth = 0.05) + 
  scale_x_continuous(breaks = seq(from = round(min(Cluster2$sil_width), digits = 1), to = round(max(Cluster2$sil_width), digits = 1), by = 0.1)) + 
  geom_vline(xintercept = c(mean(Cluster2$sil_width), median(Cluster2$sil_width), quantile(Cluster2$sil_width, probs = 0.25, names = F), quantile(Cluster2$sil_width, probs = 0.75, names = F)), color = c("red", "blue", "black", "black")) + 
  annotate("text",
           x = c(mean(Cluster2$sil_width), median(Cluster2$sil_width), quantile(Cluster2$sil_width, probs = 0.25, names = F), quantile(Cluster2$sil_width, probs = 0.75, names = F))-0.01, 
           y = c(1,1,1,1), 
           label = c("Mean", "Median", "1stQu.", "3rdQu."), 
           size = 4, angle = 90) +
  ggtitle("Distribution of silhouette")
dev.off()

p2 <- plot_ly(type = "scatter3d", x = Cluster2$DIM1, y = -Cluster2$DIM2, z = Cluster2$DIM3, mode = "markers", text = Cluster2$sample, 
              color = Cluster2$cluster, colors = pal_npg(palette = c("nrc"))(res.hc$nbclust), 
              marker = list(opacity = 0.9, line = list(color = "transparent", width = 0)), 
              hovertext = paste("Sample:", Cluster2$sample,
                                "\nSilhouette width:", round(Cluster2$sil_width, digits = 2), 
                                "\nBest neighbour:", Cluster2$neighbor),
              hoverinfo = "text+name" 
)%>%
  layout(scene = list(xaxis = list(title = 'Dim1', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, showbackground = TRUE, showline = TRUE, showticklabels = FALSE),
                      yaxis = list(title = 'Dim2', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, showbackground = TRUE, showline = TRUE, showticklabels = FALSE),
                      zaxis = list(title = 'Dim3', backgroundcolor = "rgb(242, 242, 242)", nticks = 0, tick0 = 0, mirror = TRUE, showbackground = TRUE, showline = TRUE, showticklabels = FALSE)), 
         title = paste("&nbsp;&lt;b&gt;Figure S8:&lt;/b&gt; Hierarchical clustering, R = ", dim, ", S = ", round(MDS.KS.DS2$stress/100, digits = 2), "(n = ", dim(Cluster2)[1], ", k >= 30)"), 
         paper_bgcolor = "rgb(230, 230, 230)")
api_create(p2, filename = "Cadom-HCA", fileopt = "overwrite", sharing= "private")




