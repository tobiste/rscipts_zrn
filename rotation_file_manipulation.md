---
title: "PHANEL"
author: "Tobias Stephan"
date: "12-08-2019"
output: 
  html_document: 
    keep_md: yes
    self_contained: no
    toc: yes
---


***
## Plan
1. ~~Polygone aus rotfile *Kroner540_250*~~
    + ~~ids für polygone umbenennen~~
2. ~~Pangea-Rekonstruktion für 250 Ma EEC Koordinaten~~
    + ~~rotfiles für Pangea von *Stephan*~~
3. Kleinkreise für jede Rotation an EEC ranhängen
4. fix zu EEC backrot Oslo dann backrot Paleotehys --- 400 Ma
5. backrot Panthalassa 
    + Siberia bis Silur (Scandian orogeny, 443 Ma Massextinction)
    + Silur bis Cambrium NAC
6. backrot Gondwana 


## Rotationsfile Pannotia mit Baltica als fixe Platte

**Ziel**: Die 250 Ma Pangea-Konstellation von *Greiner & Neugebauer (2013, IJES)* aus meinem `Pangea.rot` file umwandeln in fixed to Baltica

### Vorbereitung

Auf Grundlage des `Pangea4.rot` file wird in **GPLATES** die EEC Plate (*301*) als anchored plate gesetzt. Die finite rotation aller Platten relative zu EEC bei *250 Ma* werden über 
`View Total Reconstruction Poles` > `Equivalenr Rotations rel. Anchored Plate` als `*.csv ` (semi-kolon getrennt) exportiert.


### Weiterbearbeitung in R

Importieren in R.
**Indeterminate** Werte werden erstmal auf `NA` gesetzt.


```r
df <- read.table("//zfs1.hrz.tu-freiberg.de/fak3geo/GEM/Tobias Stephan/GPLATES/Pannotia/Pangea-250Ma_fixed301.csv",
                     header = F, sep=";", dec=",", na.strings = "Indeterminate") %>% 
  as_tibble() %>% 
  rename(PLATE=V1, LAT=V2, LON=V3, ANGLE=V4) %>% # header-Bezeichnungen ändern
  replace_na(list(LAT = 0, LON = 0, ANGLE = 0)) # NA Values (Indeterminate) durch 0 ersetzen
```

Da nun einige Spalte, Einträge und die Information, dass EUR die fixe Platte ist, fehlen, werden diese in **R** nachgetragen.
**Indeterminate** wird durch **0** ersetzt.
Zudem werden überflüssige Platten aus dem file entfernt.

```r
pangea250 <- df %>% # 
  add_column(AGE = 250, FIXED=301, SEP="!", 
             COMMENT=as.character("relative to EEC")) # fehlende Spalten AGE und FIXED etc hinzufügen

pangea1000 <- df %>% # 
  add_column(AGE = 1000, FIXED=301, SEP="!", 
             COMMENT=as.character("relative to EEC")) # alle platten fix zu EUR bis 1000 Ma
```


```r
pangea <- bind_rows(pangea250, pangea1000) %>% 
  select(PLATE, AGE, LAT, LON, ANGLE, FIXED, SEP, COMMENT) %>% # Reihenfolge anpassen
  filter(
           PLATE==701 | # AFR
           PLATE==301 | # EEC (EUR oder Baltica)
           PLATE==399 | # SIB
           PLATE==201 | # SAM
           PLATE==101 | # NAM
           PLATE==102 | # Groenland
           PLATE==501 | # IND
           PLATE==801 | # Australia
           PLATE==802 | # Antarctica
           PLATE==304 | # Iberia
           PLATE==503 | # Arabia
           PLATE==602 | # S-China
           PLATE==601 | # N-China
           PLATE==130 | # Avalonia
           PLATE==316 | # Britain
           PLATE==392 | # E-Avalonia
           PLATE==391 | # Armorica
           PLATE==309 | # Svalbard
           PLATE==702) %>% # Madagascar
  add_row(PLATE=301, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=0, SEP="!",
          COMMENT="EUR fixed") %>% # EUR als fixed plate auf 0 0 0 0  setzen
  add_row(PLATE=301, AGE=1000, LAT=0, LON=0, ANGLE=0, FIXED=0, SEP="!", 
          COMMENT="EUR fixed") %>% # EUR als fixed plate auf 1000 0 0 0  setzen
  add_row(PLATE=701, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT=" AFR relative to EEC") %>%
  add_row(PLATE=399, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="SIB relative to EEC") %>%
  add_row(PLATE=201, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="SAM relative to EEC") %>%
  add_row(PLATE=101, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="NAM relative to EEC") %>%
  add_row(PLATE=102, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="Groenland relative to EEC") %>%
  add_row(PLATE=501, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="IND relative to EUR") %>%
  add_row(PLATE=802, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="Antarctica relative to EEC") %>%
  add_row(PLATE=801, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="Australia relative to EEC") %>%
  add_row(PLATE=304, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="Iberia relative to EEC") %>%
  add_row(PLATE=503, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="Arabia relative to EEC") %>%
  add_row(PLATE=602, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="S-China relative to EEC") %>%
  add_row(PLATE=601, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="N-China relative to EEC") %>%
  add_row(PLATE=130, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="Avalonia relative to EEC") %>%
  add_row(PLATE=316, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="Britain relative to EEC") %>%
  add_row(PLATE=392, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="E-Avalonia relative to EEC") %>%
  add_row(PLATE=391, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="Armorica relative to EEC") %>%
  add_row(PLATE=309, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="Svalbard relative to EEC") %>%
  add_row(PLATE=702, AGE=0, LAT=0, LON=0, ANGLE=0, FIXED=301, SEP="!", COMMENT="Madagascar relative to EEC") %>%
  arrange(PLATE, AGE) # Sortieren
```

### Speichern

Als *.rot file abspeichern

```r
write.table(pangea, "//zfs1.hrz.tu-freiberg.de/fak3geo/GEM/Tobias Stephan/GPLATES/Pannotia/Pannotia0.rot", 
            quote = F, row.names = F, col.names = F, sep = "\t")
```
fertig!

> *Pannotia0.rot* sollte nicht in GPLATES oder einem Editor bearbeitet werden, 
da beim Kompilieren des R-codes diese Datei überschrieben wird. 
Also eine Kopie von *Pannotia0.rot* anfertigen (*Pannotia.rot*) und mit dieser weiterarbeiten. 


## Eulerpole  
Euler poles and reconstructed angles:

Euler pole | Lat [°] | Long [°] | Angle [°] | Age span [Ma]
--- | --- | --- | --- | ---
Neo-Tethys | 61 | 11 | 20 | 300--250
Paleo-Tethys | 33 | 28 | 17 | 370--300
?Mississippi? | ? | ? | ? | >370
Paleo-Arctic (SIB-EEC) | 77.7 | 61.7 | 60  | 540--370
Paleo-Arctic (NAM-EEC) | 77.7 | 61.7 | 50  | 540--400

rel. to EEC



## Calculation of Eulerpole and -angle from finite rotation???
