# shiny plot
shiny.plot <- function(){
  ui <- fluidPage(
    theme = shinytheme("flatly"),
    h3("Provenance end-member identification"),
    navbarPage("ProvClustR",
          tabPanel("1. Input Metadata",
                   sidebarLayout(
                     sidebarPanel(
                       fileInput("meta.file", "Choose CSV File",
                                 accept = c("text/csv", "text/comma-separated-values,text/plain", ".csv")),
                       tags$hr(),
                       checkboxInput("header1", "Header", TRUE),
                       radioButtons("sep1", "Separator",
                                    c(Comma=",",
                                      Semicolon=";",
                                      Tab="\t"),
                                    "Comma"),
                       radioButtons("quote1", "Quote",
                                    c(None="",
                                      "Double Quote"='"',
                                      "Single Quote"="'"),
                                    "Double Quote"),
                       radioButtons("dec1", "Decimal point",
                                    c("Dot"=".",
                                      "Comma"=","),
                                    "Dot"
                                    )
                     ),
                     mainPanel(
                       em("preview of first rows"),
                       tableOutput(outputId = "meta.file.output")
                     )
                   )
                   ),     
           
          tabPanel("2. Input Age data",
                    sidebarLayout(
                      sidebarPanel(
                        fileInput("age.file", "Choose CSV File",
                                  accept = c("text/csv", "text/comma-separated-values,text/plain", ".csv")),
                        tags$hr(),
                        checkboxInput("header2", "Header", TRUE),
                        radioButtons("sep2", "Separator",
                                     c(Comma=",",
                                       Semicolon=";",
                                       Tab="\t"),
                                       "Comma"),
                        radioButtons("quote2", "Quote",
                                     c(None="",
                                       "Double Quote"='"',
                                       "Single Quote"="'"),
                                     "Double Quote"),
                        radioButtons("dec2", "Decimal point",
                                     c("Dot"=".",
                                       "Comma"=","),
                                     "Dot"
                        )
                        ),
                      mainPanel(
                        em("preview of first rows"),
                        tableOutput(outputId = "age.file.output")
                      )
                    )
                    ),
           
           tabPanel("3. Pre-Processing",
                    sidebarLayout(
                      sidebarPanel(
                        numericInput("limit.young", "Minimum depositional age (Ma)",
                                     min = 0, max = 4500, step = 1, value = 0),
                        
                        numericInput("limit.old", "Maximum depositional age (Ma)",
                                     min = 0, max = 4500, step = 1, value = 4500),
                        tags$hr(),
                        
                        numericInput("cut.off", "Exclude ages younger than",
                                     min = 0, max = 4500, step = 1, value = 0),
                        tags$hr(),
                        
                        numericInput("cut.off", "Smallest amount of grains per sample",
                                     min = 2, step = 1, value = 117),
                        tags$hr(),
                        
                        checkboxInput("reference", "Include reference samples")
                      ),
                      mainPanel(
                        h5("Summary of Metadata"),
                        h6("Unfiltered"),
                        verbatimTextOutput("summary.meta0"),
                        h6("Filtered"),
                        verbatimTextOutput("summary.meta1"),
                        h5("Summary of Age data"),
                        h6("Unfiltered"),
                        verbatimTextOutput("age.overview0"),
                        h6("Filtered"),
                        verbatimTextOutput("age.overview1"),
                        h5("KDE of entire dataset"),
                        plotlyOutput("Plot0")
                      )
                    )
                    ),
           
           tabPanel("4. Plot",
                    h5(
                      paste("Provenance of all meta-sedimentary rocks \n(", limit.old, "-", limit.young,
                            "Ma) without ages younger than", cut.off, "(n = ", n, ", k >=", minimum, ")")
                    ),
                    sidebarLayout(
                      sidebarPanel(
                        selectInput("diss.type", "Dissimilarity measure:",
                                    choices = c(
                                      "Kolmogorov-Smirnov statistic", 
                                      "Kuiper statistic",
                                      "Sircombe-Hazelton's L2-distance"
                                      )
                                    ),
                        selectInput("mds.type", "Multi-dimensional scaling method:",
                                    choices = c(
                                      "Kruskal's non-metric MDS", 
                                      "Classical (metric) MDS", 
                                      "Sammon's non-linear mapping"
                                      )
                                    ),
                        sliderInput("cluster.pts", "Minimum points of cluster:",
                                    min = 2, max = 10, value = 5),
                        selectInput(
                          "coloring", "MDS coloring:", 
                          choices = c(
                            "Zircon provinces", 
                            "HDBSCAN cluster", 
                            "Depositional age"
                            )
                          ),
                        h6("Metadata of click sample"),
                        verbatimTextOutput("click"),
                        selectInput(
                          "kernel", 
                          "Smoothing kernel:", 
                          choices = c(
                            "gaussian", 
                            "rectangular", 
                            "triangular", 
                            "epanechnikov", 
                            "biweight", 
                            "cosine", 
                            "optcosine"
                            )
                          ),
                        sliderInput(
                          "bw", 
                          "Kernel smoothing bandwidth:",
                          min = 1, 
                          max = 100, 
                          value = 10
                          )
                      ),
                      mainPanel(
                        h6("Multi-dimensional scaling:"),
                        plotlyOutput("Plot1", height = "350px"),
                        h6("KDE of hover sample:"),
                        plotlyOutput("Plot2", height = "130px"),
                        h6("KDE of click sample (gray - entire age spectra):"),
                        plotlyOutput("Plot3", height = "150px")
                      )
                    ),
                    em("Stephan, T., Kroner, U. and Romer, R. L. (2019): The pre-orogenic detrital zircon record of the Peri-Gondwana crust. Geological Magazine 156(2), pp. 281-307", align="center")
           ),
           
           tabPanel("Help")
    )
  )
  
  
  
  
  
  
  
  server <- function(input, output) {
    
    options(shiny.maxRequestSize=20*1024^2) # max file size is 20 Mb
    
    # reading data from csv input
    meta.input <- reactive({
      inFile <- input$meta.file
      if(is.null(inFile))
        return(NULL)
      
      tbl <- read.csv(inFile$datapath, header=input$header1, sep=input$sep1, quote=input$quote1, dec=input$dec1)
      return(tbl)
    })
    
    output$meta.file.output <- renderTable({
      head(meta.input())
    })
    
    
    
    ages.input <- reactive({
      inFile <- input$age.file
      if(is.null(inFile))
        return(NULL)
      
      tbl <- read.csv(inFile$datapath, header=input$header2, sep=input$sep2, quote=input$quote2, dec=input$dec2)
      return(tbl)
    })
    
    output$age.file.output <- renderTable({
      head(ages.input())
    })
    
    
    # output pre-processing
   output$summary.meta0 <- renderText({
      table1 <- meta.input()
      if(is.null(table1))
        return(NULL)
      table1 <- subset(table1, !is.na(table1$SampleAge))
      
      tab1.age <- table1$SampleAge
      paste(
        "Min:", min(tab1.age),
        "| Median:", median(tab1.age),
        "| Mean:", round(mean(tab1.age), digits = 0),
        "| Max:", max(tab1.age),
        "| n samples:", length(unique(table1$Sample))
      )
    })
    
   output$summary.meta1 <- renderText({
     table2 <- meta.input()
     if(is.null(table2))
       return(NULL)
     table2 <- subset(table2, !is.na(table2$SampleAge))
     table2 <- subset(table2, table2$SampleAge <= input$limit.old)
     table2 <- subset(table2, table2$SampleAge >= input$limit.young)

     paste(
       "Min:", min(table2$SampleAge),
       "| Median:", median(table2$SampleAge),
       "| Mean:", round(mean(table2$SampleAge), digits = 0),
       "| Max:", max(table2$SampleAge),
       "| n samples:", length(unique(table2$Sample))
     )
   })
    
    
    # output plots
    output$Plot1 <- renderPlotly({
      
      if (input$diss.type == "Kolmogorov-Smirnov statistic"){
        diss.matrix <- diss.matrix.ks
        if (input$mds.type == "Kruskal's non-metric MDS"){
          MDS <- nMDS.ks; stress <- stress.nMDS.ks
          }
        if (input$mds.type == "Classical (metric) MDS"){
          MDS <- cMDS.ks; stress <- stress.cMDS.ks
          }
        if (input$mds.type == "Sammon's non-linear mapping"){
          MDS <- sammon.ks; stress <- stress.sammon.ks
          }
      }
      if (input$diss.type == "Kuiper statistic"){
        diss.matrix <- diss.matrix.kuiper
        if (input$mds.type == "Kruskal's non-metric MDS"){
          MDS <- nMDS.kuiper; stress <- stress.nMDS.kuiper
          }
        if (input$mds.type == "Classical (metric) MDS"){
          MDS <- cMDS.kuiper; stress <- stress.cMDS.kuiper
          }
        if (input$mds.type == "Sammon's non-linear mapping"){
          MDS <- sammon.kuiper; stress <- stress.sammon.kuiper
          }
      }
      if(input$diss.type=="Sircombe-Hazelton's L2-distance"){
         diss.matrix <- diss.matrix.sh
         if(input$mds.type=="Kruskal's non-metric MDS"){
           MDS <- nMDS.sh; stress <- stress.nMDS.sh
           }
         if(input$mds.type=="Classical (metric) MDS"){
           MDS <- cMDS.sh; stress <- stress.cMDS.sh
           }
         if(input$mds.type=="Sammon's non-linear mapping"){
           MDS <- sammon.sh; stress <- stress.sammon.sh
           }
      }
      
      # Clustering #############################################################
      Cluster <- hdbscan(
        diss.matrix, 
        minPts = input$cluster.pts, 
        gen_simplified_tree = F, 
        gen_hdbscan_tree = F
        ) # Hierarchical DBSCAN of Kolmogorov-Smirnov distances
      
      # zuweisen der cluster parameter zu den mds results
      
      mds <- data.frame(
        MDS.X = MDS$points[,1], 
        MDS.Y = MDS$points[,2], 
        MDS.Z = MDS$points[,3],
        sample = names(DZages), 
        HDBSCAN.Cluster = as.character(Cluster$cluster), 
        Prob = Cluster$membership_prob,
        stringsAsFactors = F, row.names = NULL
        )
      
      # zuweisen weiterer metadaten
      
      mds2 <- merge(mds, meta.df, all.x = T, by.x = "sample", by.y = "sample")
      mds2$ZrnProvince[is.na(mds2$ZrnProvince)] <- "unknown"
      mds2$CadZrnProvince[is.na(mds2$CadZrnProvince)] <- "unknown"
      
      
      mds2$HDBSCAN.Cluster[mds2$HDBSCAN.Cluster == 0] <- "outlier"
      #write.csv(mds2, "hbscan_results.csv", dec = ".")
      mds2.ref <- subset(
        mds2, 
        mds2$RefSample == T  & mds2$ZrnProvince != "reference"
        )
      
      mds2.nref <- subset(
        mds2, 
        mds2$RefSample != T & mds2$ZrnProvince != "reference"
        )
      if(reference_sample == T){
        mds2.refref <-subset(mds2, mds2$ZrnProvince == "reference")
        }
      
      key1 <- mds2.ref$sample
      key2 <- mds2.nref$sample
      
      if(input$coloring == "Zircon provinces"){
        p1 <- plot_ly(type = "scatter3d", mode="markers", colors = col.pal)
        p1 <- p1 %>% layout(
          legend = list(x = 1000, y = 0.5),
          scene = list(
            xaxis = list(
              title = 'Dim1', 
              backgroundcolor = "rgb(242, 242, 242)", 
              nticks = 0, tick0 = 0, 
              mirror = TRUE,
              showbackground = TRUE, 
              showline = TRUE, 
              showticklabels = FALSE
            ),
            yaxis = list(
              title = 'Dim2', 
              backgroundcolor = "rgb(242, 242, 242)", 
              nticks = 0, tick0 = 0, 
              mirror = TRUE,
              showbackground = TRUE, 
              showline = TRUE, 
              showticklabels = FALSE),
            zaxis = list(
              title = 'Dim3', 
              backgroundcolor = "rgb(242, 242, 242)", 
              nticks = 0, tick0 = 0, 
              mirror = TRUE,
              showbackground = TRUE, 
              showline = TRUE, 
              showticklabels = FALSE
              )
            ),
          title = paste(
            input$diss.type, "\n", input$mds.type,  "(stress = ", stress, ")"
            ),
          paper_bgcolor = "rgb(230, 230, 230)"
        )
        p1 <- p1 %>% add_markers(
          p1, 
          x = mds2.ref$MDS.X, 
          y = mds2.ref$MDS.Y, 
          z = mds2.ref$MDS.Z, 
          text = mds2.ref$sample,
          color = mds2.ref$ZrnProvince, colors = col.pal, 
          key = ~key1,
          marker = list(
            symbol="diamond", 
            size = 2, 
            opacity = 0.9, 
            line = list(color = "black", width = 0.5)
            ),
          hovertext = paste(
            "Sample:", mds2.ref$sample,
            "\nReference:", mds2.ref$Reference,
            "\nAge:", mds2.ref$Sample.age, "Ma"
            ),
          hoverinfo = "text+name"
          )
        
        p1 <- p1 %>% add_markers(
          p1, 
          x = mds2.nref$MDS.X, 
          y = mds2.nref$MDS.Y, 
          z = mds2.nref$MDS.Z, 
          text = mds2.nref$sample,
          color = mds2.nref$ZrnProvince, 
          colors = col.pal, 
          key = ~key2,
          marker = list(
            size = 4, 
            opacity = 1, 
            line = list(color = "black", width = 0.5)
            ),
          hovertext = paste("Sample:", mds2.nref$sample,
                            "\nReference:", mds2.nref$Reference,
                            "\nAge:", mds2.nref$Sample.age, "Ma"
                            ),
          hoverinfo = "text+name"
          )
      }
      if (input$coloring=="HDBSCAN cluster"){
        p1 <- plot_ly(type = "scatter3d", mode="markers")
        p1 <- p1 %>% layout(legend = list(x = 1000, y = 0.5),# legend position
                            scene = list(
                              xaxis = list(
                                title = 'Dim1', 
                                backgroundcolor = "rgb(242, 242, 242)", 
                                nticks = 0, 
                                tick0 = 0, 
                                mirror = TRUE,
                                showbackground = TRUE, 
                                showline = TRUE, 
                                showticklabels = FALSE
                                ),
                              yaxis = list(
                                title = 'Dim2', 
                                backgroundcolor = "rgb(242, 242, 242)", 
                                nticks = 0, 
                                tick0 = 0, 
                                mirror = TRUE,
                                showbackground = TRUE, 
                                showline = TRUE, 
                                showticklabels = FALSE
                                ),
                              zaxis = list(
                                title = 'Dim3', 
                                backgroundcolor = "rgb(242, 242, 242)", 
                                nticks = 0, tick0 = 0, 
                                mirror = TRUE,
                                showbackground = TRUE, 
                                showline = TRUE, 
                                showticklabels = FALSE
                                )
                              ),
                            title = paste(
                              input$diss.type, "\n", 
                              input$mds.type,  "(stress = ", stress, ")"
                              ),
                            paper_bgcolor = "rgb(230, 230, 230)"
                            )
        p1 <- p1 %>% add_markers(
          p1, 
          x = mds2.ref$MDS.X, 
          y = mds2.ref$MDS.Y, 
          z = mds2.ref$MDS.Z, 
          text = mds2.ref$sample,
          color = mds2.ref$HDBSCAN.Cluster, 
          key = ~key1, #colors = col.pal,
          marker = list(symbol="diamond", 
                        size = 2, 
                        opacity = 0.9, 
                        line = list(color = "black", width = 0.5)
                        ),
          hovertext = paste("Sample:", 
                            mds2.ref$sample,    
                            "\nReference:", mds2.ref$Reference,
                            "\nAge:", mds2.ref$Sample.age, "Ma",
                            "\nMembership prob.:", 
                            round(mds2.ref$Prob, digits = 2)#
                            ),
          hoverinfo = "text+name", 
          showlegend = F
          )
        
        p1 <- p1 %>% add_markers(
          p1, 
          x = mds2.nref$MDS.X, 
          y = mds2.nref$MDS.Y, 
          z = mds2.nref$MDS.Z, 
          text = mds2.nref$sample,
          color = mds2.nref$HDBSCAN.Cluster, 
          key = ~key2,
          marker = list(size = 4, 
                        opacity = 1, 
                        line = list(color = "black", width = 0.5)
                        ),
          hovertext = paste("Sample:", 
                            mds2.nref$sample,   
                            "\nReference:", mds2.nref$Reference,
                            "\nAge:", mds2.nref$Sample.age, "Ma",
                            "\nMembership prob.:", 
                            round(mds2.nref$Prob, digits = 2)#
                            ),
          hoverinfo = "text+name"
          )
      }
      if (input$coloring == "Depositional age"){
        p1 <- plot_ly(type = "scatter3d", mode = "markers", colors = col.pal.age)
        p1 <- p1 %>% layout(
          legend = list(x = 1000, y = 0.5),# legend position
          scene = list(
            xaxis = list(title = 'Dim1', 
                         backgroundcolor = "rgb(242, 242, 242)", 
                         nticks = 0, 
                         tick0 = 0, 
                         mirror = TRUE,
                         showbackground = TRUE, 
                         showline = TRUE, 
                         showticklabels = FALSE
                         ),
            yaxis = list(title = 'Dim2', 
                         backgroundcolor = "rgb(242, 242, 242)", 
                         nticks = 0, 
                         tick0 = 0, 
                         mirror = TRUE,
                         showbackground = TRUE, 
                         showline = TRUE, 
                         showticklabels = FALSE
                         ),
            zaxis = list(title = 'Dim3', 
                         backgroundcolor = "rgb(242, 242, 242)", 
                         nticks = 0, 
                         tick0 = 0, 
                         mirror = TRUE,
                         showbackground = TRUE, 
                         showline = TRUE, 
                         showticklabels = FALSE
                         )
            ),
          title = paste(input$diss.type, 
                        "\n", 
                        input$mds.type,  
                        "(stress = ", 
                        stress, ")"
                        ),
          paper_bgcolor = "rgb(230, 230, 230)"
        )
        p1 <- p1 %>% add_markers(
          p1, 
          x = mds2.ref$MDS.X, 
          y = mds2.ref$MDS.Y, 
          z = mds2.ref$MDS.Z, 
          text = mds2.ref$sample,
          color = mds2.ref$Age.class, 
          colors = col.pal.age, 
          key = ~key1,
          marker = list(symbol="diamond", 
                        size = 2, 
                        opacity = 0.9, 
                        line = list(color = "black", width = 0.5)
                        ),
          hovertext = paste("Sample:", mds2.ref$sample,
                            "\nReference:", mds2.ref$Reference,
                            "\nAge:", mds2.ref$Sample.age, "Ma"
                            ),
          hoverinfo = "text+name", 
          showlegend = F
          )
        p1 <- p1 %>% add_markers(
          p1, 
          x = mds2.nref$MDS.X, 
          y = mds2.nref$MDS.Y, 
          z = mds2.nref$MDS.Z, 
          text = mds2.nref$sample,
          color = mds2.nref$Age.class, 
          colors = col.pal.age, 
          key = ~key2,
          marker = list(size = 4, 
                        opacity = 1, 
                        line = list(color = "black", width = 0.5)
                        ),
          hovertext = paste("Sample:", mds2.nref$sample,
                            "\nReference:", mds2.nref$Reference,
                            "\nAge:", mds2.nref$Sample.age, "Ma"
                            ),
          hoverinfo = "text+name"
          )
      }
      
      if (reference_sample == T){
        p1 <- p1 %>% add_markers(
          p1, x = mds2.refref$MDS.X, 
          y = mds2.refref$MDS.Y, 
          z = mds2.refref$MDS.Z, 
          text = mds2.refref$sample,
          name = "Cluster center", 
          showlegend = T,
          marker = list(size = 3, 
                        opacity = 1, 
                        color = "black", 
                        line = list(color = "black", width = 0.5)
                        ),
          hovertext = paste("Sample:", mds2.refref$sample,
                            "\nMean Age:", 
                            round(mds2.refref$Sample.age, digits=0), "Ma",
                            "\nNumber of zircon grains (k):", 
                            mds2.refref$k.min),
          hoverinfo = "text+name") %>%
          add_text(
            p1, 
            x = mds2.refref$MDS.X, 
            y = mds2.refref$MDS.Y, 
            z = mds2.refref$MDS.Z, 
            text = mds2.refref$sample,
            textposition="top center", 
            hovertext = "none", 
            hoverinfo = "none", 
            showlegend = F
          )
        }
      }
    )
    
    output$Plot2 <- renderPlotly({
      d <- event_data("plotly_hover")
      if(is.null(d) == T) return(NULL)
      if(is.null(d) == F){
        hist2 <- subset(zrn.data, zrn.data$sample == d$key)
        hist2 <<- hist2
        p2 <- ggplot(hist2) +
          geom_density(
            aes(x = hist2$age), 
            bw = input$bw, 
            kernel = input$kernel, 
            fill = "dodgerblue4", 
            alpha=.5, 
            color="dodgerblue4",
            na.rm = T
            ) +
          labs(title = paste(
            d$key, "(Number of grains:", hist2$k.min[1], ")"), 
            x =  "age in Ma"
            ) +
          scale_x_continuous(
            limits = c(0, 3600), 
            breaks = seq(from = 0, to = 3600, by = 200)
            ) +
          theme_classic()
        p2 <- ggplotly(p2)
        p2
      }
    })
    
    output$Plot3 <- renderPlotly({
      d <- event_data("plotly_click")
      if(is.null(d) == T) return(NULL)
      if(is.null(d) == F){
        hist1 <- subset(zrn, zrn$sample == d$key)
        hist1 <<- hist1
        
        hist2 <- subset(zrn.data, zrn.data$sample == d$key)
        hist2 <<- hist2
        
        p2 <- ggplot() +
          geom_density(
            data = hist1, 
            aes(x = hist1$age), 
            bw = input$bw, 
            kernel = input$kernel, 
            fill = "gray", 
            alpha = .5, 
            color = "gray", 
            trim = T,
            na.rm = T
            ) +
          geom_density(
            data = hist2, 
            aes(x = hist2$age), 
            bw = input$bw, 
            kernel = input$kernel, 
            fill = "dodgerblue", 
            alpha = .5, 
            color = "dodgerblue", 
            trim = F,
            na.rm = T
            ) +
          labs(
            title = paste(
              d$key, "(Number of grains:", 
              hist2$k.min[1], "/", hist2$k[1], ")"), x =  "age in Ma"
            ) +
          scale_x_continuous(
            limits = c(0, 3600), 
            breaks = seq(from = 0, to = 3600, by = 200)
            ) +
          theme_classic()
        p2 <- ggplotly(p2)
        p2
      }
    })
    
    output$click <- renderText({
      d <- event_data("plotly_click")
      if (is.null(d)){
        "Click events appear here (double-click to clear)"
        } else {
        hist1 <- subset(zrn.data, zrn.data$sample == d$key)
        hist1 <<- hist1
        print(paste("Sample:", hist1$sample[1],
                    "\nReference:", hist1$reference[1],
                    "\nAge:", hist1$sample.age[1], "Ma",
                    "\nZircon province:", hist1$signat2[1],
                    "\nRock:", hist1$rock.type[1],
                    "\nLocation:", hist1$location[1],
                    "\nUnit:", hist1$unit[1],
                    "\nFormation:", hist1$formation[1],
                    "\nLongitude:", round(hist1$long[1], digits = 2),
                    "Latitude:", round(hist1$lat[1], digits = 2)#,
                    #"\nNumber of zircon grains (k):", hist1$k.min[1]
                    )
        )
        }
    }
    )
  }
  shinyApp(ui, server)
}
