---
title: "Rotation in R"
author: "Tobi"
date: "12/08/2019"
output: 
  html_document: 
    toc: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library("easypackages")
packages("pracma", "mosaic", "ppls", prompt=T)
# source() # selbstgeschriebene Funktionen mal zusammen fassen
```

***

## Basics

### Basic operations with vectors

Instance of vector object `c()` could be created from any iterable object as list, tuple or array
```{r basic1}
u <- c(1, -2, 3)
v <- c(-2, 1, 1)
```

For common vector operation we can use standard mathematical operators or special methods using dot notation
```{r basic2}
u+v
u-v
3*u - 2*v
```

### Vector length
The magnitude or length of a vector $\vec u = (u_1, u_2, u_3)$ is defined as its *Euclidean norm* (or Euclidean length, L2 distance):

$||\vec u||_2 = \sqrt{u_1^2 + u_2^2 + u_3^2}$

It can be calculated using `euclid_norm()`:
```{r euclid_norm, include=F}
  euclid_norm <- function(x){
  sqrt(sum(x^2))
  }
```

### Dot product 
The dot product (dt. *Skalarprodukt*) of two vectors results a **number**.

Geometrical: $\vec u \cdot \vec v = |\vec u| |\vec v| \cos(\vec u, \vec v)$
where $|\vec u|$ and $|\vec v|$ are the length of the vecors $\vec u$ and $\vec v$. 

if $\vec u$ is perpendicular to $\vec v$: 0

if $\vec u$ is parallel to $\vec v$: max

Cartesian: $\vec u \cdot \vec v = u_1 v_1 + u_2 v_2 + u_3 v_3$

For dot product we can use function `dot()` or `%*%`
```{r dot_product}
#pracma::dot(u, v)
mosaic::dot(u, v)
u %*% v
```

### Cross product
The cross product (dt. *Kreuzprodukt*) of two vectors results in a new **vector**.

Thereby, the cross product $\vec u \times \vec v$ between $\vec u$ and $\vec v$ is orthogonal to the two vectors ("Rechte-Hand-Regel").

$\vec u \times \vec v = \begin{pmatrix} u_1 \\ u_2 \\ u_3 \end{pmatrix} \times \begin{pmatrix} v_1 \\ v_2 \\ v_3 \end{pmatrix} = \begin{pmatrix} u_2 v_3 - u_3 v_2 \\ u_3 v_1 - u_a1 v_3 \\ u_1 v_2 - u_2 v_1 \end{pmatrix}$

For cross product we can use operator `cross()`
```{r cross_product}
pracma::cross(u,v)
```
### Transposition of a vector or a matrix
```{r transpose}
t(u)
```


### Projection
The projection $\vec u_v$ of a vector $\vec u$ on $\vec v$ is

$\vec u_v = \frac{\vec u \cdot \vec v}{|\vec v|^2}\vec v$

To project vector *u* onto vector *v* we can use method `project()`:
```{r projection}
#library("mosaic")
project(u, v)
```

### Angle between vectors
The angle $\theta$ between two vectors $\vec u$ and $\vec v$ is solved by knowledge of the dot product and the length of the two vectors:

$\theta = arccos(\frac{\vec u \cdot \vec v}{|\vec u| |\vec v|})$

To find the angle (in **degrees**) between two vectors we use the method `angle()` (or `angle0()`)
```{r angle_funct, include=F}
angle0 <- function(x, y){
  dot.prod <- x %*% y 
  norm.x <- norm(x, type = "2")
  norm.y <- norm(y, type = "2")
  theta <- acos(dot.prod / (norm.x * norm.y))
  rad2deg(as.numeric(theta))
}

angle <- function(x, y){
  rad2deg( acos(sum(x * y) / (sqrt(sum(x * x)) * sqrt(sum(y * y)))))
  #return(rad2deg(angle))
}
```

```{r angle, include=T}
angle(u, v)
```
***

## Coordinate system conversions
To operate with polar coordinates, we need to convert these coordinates (always) in cartesian coordinates
```{r convers_funct, include=F}
to_polar <- function(n) {
  r <- euclid_norm(n)
  lat <- rad2deg(asin(n[3]/r))
  lon <- rad2deg(atan2(n[2], n[1]))
  
  #az <- atan2d(n[2], n[1]) 
  #dip <- asind(n[3] / len)
  if (lat < 0) {
        lat <- 90 - lat
        #az = az + 180
    }
  return(c(lat, lon))
}

to_cartesian <- function(p, r) {
  if(missing(r)){
    r <- 1
    }
  x <- c()
  #t <- cosd(p[2])
  x[1] <- r * cosd(p[1]) * cosd(p[2]) 
  x[2] <- r * cosd(p[1]) * sind(p[2]) 
  x[3] <- r * sind(p[1])
  x <- normalize.vector(x)
  return(x)
}
```

### Cartesian to polar coordinates 
```{r convers1}
u.polar <-to_polar(u)
u.polar
```

### Polar to cartesian coordinates 

(optional: `to_cartesian(x, r)` with r = Earth´s radius, is 1 by default)
```{r convers2}
to_cartesian(u.polar) 
```

***

## Rotation
If $\vec u$ is a vector prior to rotation
and $\vec u'$ is the point after rotation
  
then 
  
$\vec u' = R \cdot \vec u$ 
  
where $R$ is a 3x3 **rotation matrix**: 

$R={\begin{bmatrix}\cos \psi +u_{x}^{2}\left(1-\cos \psi \right)&u_{x}u_{y}\left(1-\cos \psi \right)-u_{z}\sin \psi &u_{x}u_{z}\left(1-\cos \psi \right)+u_{y}\sin \psi \\u_{y}u_{x}\left(1-\cos \psi \right)+u_{z}\sin \psi &\cos \psi +u_{y}^{2}\left(1-\cos \psi \right)&u_{y}u_{z}\left(1-\cos \psi \right)-u_{x}\sin \psi \\u_{z}u_{x}\left(1-\cos \psi \right)-u_{y}\sin \psi &u_{z}u_{y}\left(1-\cos \psi \right)+u_{x}\sin \psi &\cos \psi +u_{z}^{2}\left(1-\cos \psi \right)\end{bmatrix}}$

### Rotation matrix
The matrix can be written more concisely as (*Rodrigues' rotation formula*): 

$R =I_3 \cos\psi  + [\vec e]_x sin\psi  + (1 -\cos\psi) \vec e \otimes \vec e$

where $\vec e$ is the unity vector of $e$, $[\vec e]_x$ is the *cross product matrix* of  $\vec e$, $\otimes$ is the *outer product* ($\vec e \otimes \vec e = \vec e \vec{e}^T$), and $I_3$ is the *identity matrix*.

> $\vec e$ and $\psi$ refer to the **Euler axis** and the **Euler angle**, respectively (see below).

To calculate the rotation matrix from a vector (in cartesian coordinates) and an angle, use `rotation_matrix()`:
```{r rotation_matrix_func, include=F}
rotation_matrix <-function(n, alpha){
  n <- normalize.vector(n) # unit vector
  R <- matrix(nrow=3, ncol=3)
    R[1, 1] <- n[1]^2 * (1 - cosd(alpha)) + cosd(alpha)
    R[1, 2] <- n[1] * n[2] * (1 - cosd(alpha)) - n[3] * sind(alpha)
    R[1, 3] <- n[1] * n[3] * (1 - cosd(alpha)) + n[2] * sind(alpha)
    R[2, 1] <- n[2] * n[1] * (1 - cosd(alpha)) + n[3] * sind(alpha)
    R[2, 2] <- n[2]^2 * (1 - cosd(alpha)) + cosd(alpha)
    R[2, 3] <- n[2] * n[3] * (1 - cosd(alpha)) - n[1] * sind(alpha)
    R[3, 1] <- n[3] * n[1] * (1 - cosd(alpha)) - n[2] * sind(alpha)
    R[3, 2] <- n[3] * n[2] * (1 - cosd(alpha)) + n[1] * sind(alpha)
    R[3, 3] <- n[3]^2 * (1 - cosd(alpha)) + cosd(alpha)
    return(R)
}

# a little bit slower, but more elegant
rotation_matrix2 <- function(e, alpha){
 e <- normalize.vector(e) # unit vector
 I <- diag(3)
 ex <- cbind(pracma::cross(e, I[, 1]),  pracma::cross(e, I[, 2]), pracma::cross(e, I[, 3])) 
 R <- (cosd(alpha)) * I + (sind(alpha)) * ex + (1 - cosd(alpha)) * outer(e, e)
 return(R) 
}
```

```{r rotation_matrix_example}
w <- c(0, 1, 0)
R <- rotation_matrix(w, 90)
#rotation_matrix2(v, 45) # Rodrigues' rotation formula: slower but more elegant
R
```


### Rotation of a vector around given vector and angle
The dot product of the rotation matrix and a vector provides the rotation of the vector:
```{r rotation_matrix_example2}
u.rot2 <- c(R%*%u) # rotation of u
u.rot2
```

**Alternatively**, method `rotate()` provides the possibility to rotate a vector around another vector. For example, to rotate vector *u* around vector *v* for 45°:
```{r rotation_func, include=F}
rotate <- function(x, n, alpha){
#alpha <- deg2rad(alpha)
 n <- normalize.vector(n)
 a <- n * dot(n, x) 
 b <- cosd(alpha) * pracma::cross(pracma::cross(n, x), n) 
 c <- sind(alpha) * pracma::cross(n, x)
 a+b+c
}
```

```{r rotation}
u.rot <- rotate(u, w, 45)
u.rot
```


### Rotation angle 
To find the angle $\psi$ of a rotation $R$ can be calculated using the *trace* $\operatorname{Tr}$, i.e., the sum of the diagonal elements of the rotation matrix ($A_{11} + A_{22} + A_{33}$). Care should be taken to select the right sign for the angle $\psi$ to match the chosen axis:

$\operatorname{Tr} (R) = 1 + 2 \cos \psi$ 

from which follows that the angle's absolute value is

$|\psi | = \arccos \left({\frac {\operatorname{Tr} (R)-1}{2}}\right)$

The angle (in degrees) is calculated using `rotation_angle()`:
```{r rotation_angle, include=F}
rotation_angle <- function(R){
  psi <- acos( (sum(diag(R)) - 1) / 2)
  return(rad2deg(psi))
}
```

```{r rotation_angle_example, include=T}
rotation_angle(R)
```

### Rotation axis
The rotation axis (or **Euler axis**) can be derived from the rotation matrix $R$:

$e_{1}={\frac {A_{32}-A_{23}}{2\sin \psi}}$

$e_{2}={\frac {A_{13}-A_{31}}{2\sin \psi}}$

$e_{3}={\frac {A_{21}-A_{12}}{2\sin \psi}}$

The axis can be calculated using `rotation_axis()` and the rotation matrix:
```{r rotation_axis, include=FALSE}
rotation_axis <- function(A){
  psi <- rotation_angle(A)
  e1 <- (A[3, 2] - A[2, 3]) / 2 * sind(psi)
  e2 <- (A[1, 3] - A[3, 1]) / 2 * sind(psi)
  e3 <- (A[2, 1] - A[1, 2]) / 2 * sind(psi)
  return(c(e1, e2, e3))
}
```

```{r rotation_axis_example}
rotation_axis(R)
```


***

### Great-circle distance
The great-circle distance or orthodromic distance is the shortest distance between two points on the surface of a sphere, measured along the surface of the sphere (as opposed to a straight line through the sphere's interior) 

Let $\lambda_{1}$, $\phi_{1}$ and $\lambda_{2}$ $\phi_{2}$ be the geographical latitude and longitude in radians of two points a and b, and $\Delta \lambda$, $\Delta \phi$ be their absolute differences; then $\Delta \sigma$, the central angle between them, is given by the spherical law of cosines if one of the poles is used as an auxiliary third point on the sphere: 

$\Delta \sigma =\arccos (\sin(\lambda _{A})\cdot \sin(\lambda _{B})+\cos(\lambda _{A})\cdot \cos(\lambda _{B})\cdot \cos(\phi _{B}-\phi _{A}))$

```{r orthodrome, include=FALSE}
orthodrome <- function(a, b){
  delta <- acosd( sind(a[1]) * sind(b[1]) + cosd(a[1]) * cosd(b[1]) * cosd(b[2]-a[2]) )
  return(delta)
}

```

```{r orthodrome_example}
p.neo250 <- c(-9.7038, -61.0314)
p.neo300 <- c(-18.2936, -42.3868)
p.pt300 <- c(-5.7285, -58.2649)
p.pt370 <- c(-19.8101, -48.4716)

orthodrome(p.neo250, p.neo300)
orthodrome(p.pt300, p.pt370)

berlin <- c(52.517, 13.4)
tokyo <- c(35.7, 139.767)
orthodrome(berlin, tokyo)

```


The problem is normally expressed in terms of finding the central angle $\Delta \sigma$. Given this angle in radians, the actual arc length d on a sphere of radius r can be trivially computed as

$d=r\,\Delta \sigma$.

```{r distance_on_sphere}
spherical_distance <- function(a, b){
  r.earth <- 6371
  d <- r.earth * deg2rad(orthodrome(a, b))
  return(d)
}
#distanz zwischen zwei punkten
spherical_distance(p.neo250, p.neo300)
spherical_distance(p.pt300, p.pt370)

#winkel zwischen zwei punkten wenn distanz bekannt
orthodrome_from_distance <- function(d, radius) {
  if(missing(radius)){
    r <- 6371
    }
  rad2deg(d/r)
}
orthodrome_from_distance(2222.68467) # NeoTethys
orthodrome_from_distance(1900.7192) # Paleo-Tethys

```










## Euler rotations in plate-tectonic reconstructions

### Mathematical formulation

#### Euler pole and Euler angle
A rotation on a sphere is defined by an **Euler pole** $\vec E$ (with latitute $\lambda$ and longitude $\phi$, or in Cartesian coordinates $\vec E = (E_x, E_y, E_z)$) and the **Euler angle** $\psi$. The angle $\psi$ is measured counterclockwise. 

```{r euler_pole_instance, include=F}
euler_pole <- function(x, y, z, type){
  ep <- data.frame(
    Lat=numeric(1), 
    Lon=numeric(1), 
    X=numeric(1), 
    Y=numeric(1), 
    Z=numeric(1))
  
  if(type=="cartesian") {
    pol  <- to_polar(c(x, y, z))
    ep$Lat <- pol[1]
    ep$Lon <- pol[2]
    ep$X <- x
    ep$Y <- y
    ep$Z <- z
    return(as_tibble(ep)) 
    } 
  if(type=="polar"){
    cart <- to_cartesian(c(x, y))
    ep$Lat <- x
    ep$Lon <- y
    ep$X <- cart[1]
    ep$Y <- cart[2]
    ep$Z <- cart[3]
    return(as_tibble(ep)) 
  }
  if(type!="polar" | type!="cartesian" | missing(type)){
    stop("Specify input coordinate system")
    }
  }
```

To define an Euler pole, use method `euler_pole()` by specifying wether the (Lat, Long) or (x, y, z) coordinates are given:
```{r euler_pole}
ep <- euler_pole(90, 0, NA, type="polar")
ep
```

#### Euler rotation
Euler pole and angle define the **Euler rotation**:

$\mathrm{Euler~rotation} = \operatorname{ROT}[\vec E, \psi]$

>For the notation of rotation parameters the conventions of *Cox and Hart (1986)* will be used, because this notation describes the parameters best. 

The point $\vec P$ can be transformed with an Euler rotation to the point $\vec{P'}$. This is performed by multiplying a rotation matrix $A$ with the vector 

$\vec{P}' = A \vec P$ 

In order to derive the rotation matrix $A$, the rotation around the Euler pole
$\vec E$ is divided into three single rotations

1. A transformation $T$ into the coordinate system, where the $\vec E$ is the unit vector along the $z$-axis $(0, 0, 1)$.
2. A rotation $R$ around the $z$-axis with the rotation angle $\psi$.
3. A transformation $T^{-1}$ back to the original coordinates, where $T^{-1}$ is the inverse of $T$.

The elements $T_{ik}$ of the matrix $T$ are called *direction cosines*. They are the cosines of the angles between the axes of the first coordinate system $(x, y, z)$ and the transformed coordinate system $(x', y', z')$:

$T = \begin{pmatrix} \cos(x, x') & \cos(y, x') & \cos(z, x') \\ \cos(x, y') & \cos(y, y') & \cos(z, y') \\ \cos(x, z') & \cos(y, z') & \cos(z, z') \end{pmatrix}$

If these elements are derived from the latitude $E_{\lambda}$ and the longitude $E_\phi$ of the Euler pole, $T$ can be written as

$T = \begin{pmatrix} \sin E_{\lambda}\cos E_{\phi} & \sin E_{\lambda}\sin E_{\phi} & -\cos E_{\lambda} \\ -\sin E_{\phi} & \cos E_{\phi} & 0 \\ \cos E_{\lambda}\cos E_{\phi} & \cos E_{\lambda}\sin E_{\phi} & \sin E_{\lambda} \end{pmatrix}$

The rotation $A$ around the transformed Euler pole with the angle $\psi$ is a simple rotation around the $z$-axis

$R = \begin{pmatrix} \cos \psi & -\sin \psi & 0 \\ \sin \psi & \cos \psi & 0 \\ 0 & 0 & 1\end{pmatrix}$

Because the matrix $T$ is a transformation of an orthonormal basis, $T$ is an orthogonal matrix. The inverse $T^{-1}$ is equal to the transposed matrix $T^T$ and can be derived by switching the rows and columns of $T$. When these three transformations are added, the total rotation will become 

$A = T^{-1}R T$

and if th ematrix elements are multiplied:

$A = \begin{pmatrix} E^2_x(1-\cos \psi) + \cos \psi & E_xE_y(1-\cos\psi)-E_z\sin\psi & E_xE_z(1-\cos\psi)+E_y\sin\psi \\ E_yE_x(1-\cos\psi)+E_z\sin\psi & E^2_y(1-cos\psi)+cos\psi & E_yE_z(1-\cos\psi)-E_x\sin\psi \\ E_zE_x(1-\cos\psi)-E_y\sin\psi & E_zE_y(1-\cos\psi)+E_x\sin\psi & E_z^2(1-\cos\psi)+\cos\psi\end{pmatrix}$


```{r euler_rot, include=FALSE}
euler_rot <- function(e, psi){
  mat <- matrix(nrow=3, ncol=3)
  mat[1, 1] <- sind(e$Lat) * cosd(e$Lon)
  mat[1, 2] <- sind(e$Lat) * sind(e$Lon)
  mat[1, 3] <- -1 * cosd(e$Lat)
  mat[2, 1] <- -1 * sind(e$Lon)
  mat[2, 2] <- cosd(e$Lon)
  mat[2, 3] <- 0
  mat[3, 1] <- cosd(e$Lat) * cosd(e$Lon)
  mat[3, 2] <- cosd(e$Lat) * sind(e$Lon)
  mat[3, 3] <- sind(e$Lat)

  R <- matrix(nrow=3, ncol=3)
  R[1, 1] <- cosd(psi)
  R[1, 2] <- -1 * sind(psi)
  R[1, 3] <- 0
  R[2, 1] <- sind(psi)
  R[2, 2] <- cosd(psi)
  R[2, 3] <- 0
  R[3, 1] <- 0
  R[3, 2] <- 0
  R[3, 3] <- 1
  
  A <- solve(mat) %*% R %*% mat
  return(A)
}
```

The Euler rotation matrix can be calculated with `euler_rot()` using an Euler pole and an Euler angle:
```{r euler_rot_example}
euler.rot <- euler_rot(ep, 90)
euler.rot
```

Multiplying the point $P'$ with the rotation matrix $A$ will result in the point $P$
```{r rot}
point <- c(0, 0)
point1 <- to_polar(euler.rot %*% to_cartesian(point))
point1
```


#### Back rotation
The point $\vec{P'}$ can be transformed **back** to $\vec P$ with the Euler rotation $\vec P = A^{-1}\vec{P'} = \operatorname{ROT}[\vec E, -\psi] \vec{P'}$.
Therefore it is useful to define $\operatorname{ROT}[\vec E, -\psi]$ as **negative rotation** of  $\operatorname{ROT}[\vec E, \psi]$:

$\operatorname{ROT}[\vec E, -\psi] = -\operatorname{ROT}[\vec E, \psi]$.

The back rotation of a point can be calculate using the dot product between the inverse (`solve()`) Euler rotation matrix $A^{-1}$ and the point (vector in cartesian coordinates):

```{r backrot}
euler.rot.inv <- solve(euler.rot)  # get the inverse matrix
to_polar(euler.rot.inv %*% to_cartesian(point1)) # back rotated point in polar coordinates
```



#### Inverse Euler pole
As an Euler pole is the intersection of the rotation axis with the surface of the sphere through the sphere's center, an inverse Euler pole
$\vec{E'}$ exists on the other side of the sphere. The point $\vec{P}$ can also be transformed to $\vec{P'}$ by using the inverse pole $\vec{E'}$ and the negative rotation angle $-\psi$. The inverse pole $\vec{E'}$ can be derived by

$E_{\lambda}' = -E_{\lambda}$ and $E_{\phi}' = E_{\phi} + 180°$.


and therefore

$\operatorname{ROT}[\vec E, \psi] = \operatorname{ROT}[\vec{E'}, -\psi] = -\operatorname{ROT}[\vec{E'}, \psi] = -\operatorname{ROT}[\vec E, -\psi]$.

```{r inverse_euler_pole, include=FALSE}
inverse_euler_pole <- function(euler.pole){
  Lat.inv <- -1 * euler.pole$Lat
  Lon.inv <- euler.pole$Lon + 180
  ep.inv <- euler_pole(Lat.inv, Lon.inv, NA, "polar")
  return(ep.inv)
}
```

Function `inverse_euler_pole()` provides the inverse of the Euler pole:
```{r inverse_euler_pole_example}
inverse_euler_pole(ep)
```

#### Euler pole extraction from two points
The Euler pole from responsible for the roation of point $P$ to $P'$:
```{r euler_from_points, include=FALSE}
euler_from_points <- function(p, q) {
  e <- pracma::cross(to_cartesian(q), to_cartesian(p))
  euler.pole <- euler_pole(e[1], e[2], e[3], "cartesian")
  #euler.pole <- inverse_euler_pole(euler.pole)
  
  #a <- angle(to_cartesian(p), to_cartesian(q))
  a <- orthodrome(p, q)
  rot <- euler_rot(euler.pole, a)
  
  return(list(euler.pole=euler.pole, psi=a, rotation.matrix=rot))
} 
```
```{r euler_from_points_example}
euler_from_points(point1, point)
```



#### Addition of Euler rotations 

Because of Euler's theorem, an addition (consecutive operation) of two Euler rotations can also be described by a single rotation

$\operatorname{ROT}[\vec E_1, \psi_1] + \operatorname{ROT}[\vec E_2, \psi_2] = \operatorname{ROT}[\vec{E}_3, \psi_3]$

Note that the rotation subscripted 2 is applied first. This is done by multiplying the matrices $A_1$ and $A_2$

$A_1 A_2 = A_3$

For example:
```{r rotation_addition}
A3 <- euler_rot(ep, 90) %*% euler_rot(ep, 45)
A3
```

>The summation of finite rotations or matrix products is **not commutative**:
>$\operatorname{ROT}[\vec E_1, \psi_1] + \operatorname{ROT}[\vec E_2, \psi_2] \neq \operatorname{ROT}[\vec E_2, \psi_2] + \operatorname{ROT}[\vec E_1, \psi_1]$

The Euler rotation $\operatorname{ROT}[\vec{E}_3, \psi_3]$ can be derived from the resulting matrix $A_3$. 

$A_{32} - A_{23} = 2 E_x \sin \psi$

$A_{13} - A_{31} = 2 E_y \sin \psi$

$A_{21} - A_{12} = 2 E_z \sin \psi$

$A_{11} + A_{22} + A_{11}  = 2 \cos \psi$

from which we can derive

$E_{\lambda} = \sin^{-1} (\frac{ A_{21}-A_{12}} {\sqrt{(A_{32}-A_{23})^2 + (A_{12}-A_{31})^2 + (A_{21}-A_{12})^2}})$

$E_{\phi} = \tan^{-1} (\frac{A_{13}-A_{31}}{A_{32}-A_{23}})$

$\psi = \tan^{-1} ( \frac{ \sqrt{(A_{32}-A_{23})^2 + (A_{12}-A_{31})^2 + (A_{21}-A_{12})^2} } {A_{11} + A_{22} + A_{33} - 1} )$


```{r euler_from_rot, include=FALSE}
euler_from_rot <- function(A){

  psi <- rotation_angle(A)
  ra <- rotation_axis(A)
  e <- euler_pole(ra[1], ra[2], ra[3], "cartesian")
  return(list(euler.pole=e, psi=psi))
  
  #e.lat <- 1/ sin( (A[2, 1] - A[1, 2]) / sqrt((A[3, 2] - A[2, 3])^2 + (A[1, 2] - A[3, 1])^2 + (A[2, 1] - A[1, 2])^2))
  #e.lon <- 1 / tan((A[1, 3] - A[3, 1]) / (A[3, 2] - A[2, 3]))
  #psi <-  1 / tan( sqrt( (A[3, 2] - A[2, 3])^2 + (A[1, 2] - A[3, 1])^2 + (A[2, 1] - A[1, 2])^2) / (A[1, 1] + A[2, 2] + A[3, 3] - 1))
  
  #e <- euler_pole(rad2deg(e.lat), rad2deg(e.lon), NA, "polar")
  
  #return(list(euler.pole=e, psi=rad2deg(psi)))
  
}
```

```{r euler_from_rot_example}
euler_from_rot(euler.rot)
euler_from_rot(A3)
```


##### Extrinsic Euler angles (z-x-z) --- rotation matrix
Using the x-convention, the 3-1-3 **extrinsic Euler angles** $\phi$, $\theta$ and $\psi$ (around the $z$-axis, $x$-axis and again the $z$-axis) can be obtained as follows: 

$\phi =\operatorname {atan2} (A_{31},A_{32})$

$\theta =\arccos (A_{33})$

$\psi =-\operatorname {atan2} (A_{13},A_{23})$

```{r extrinsic_euler_from_rot, include=FALSE}
extr_euler_from_rot <- function(A){
  lat <- atan2(A[3, 1], A[3, 2])
  lon <-  acos(A[3, 3])
  psi <- - atan2(A[1, 3], A[2, 3])
  
  e <- euler_pole(rad2deg(lat), rad2deg(lon), NA, "polar")
  return(list(e, rad2deg(psi)))
}
```
```{r extrinsic_euler_from_rot_example, include=T}
extr_euler_from_rot(euler.rot)
```




### Finite Euler rotations
It has been shown how a single point rotates with an Euler rotation on a sphere. This is also possible for groups of points, defining the shorelines or boundaries of continents or terranes.
When using continents or terranes, the Euler rotation $\operatorname{ROT}[\vec E, \psi]$ is moving the plate B with respect to A for a given time $t$ with respect to the present day position ${}^0_A\operatorname{ROT}^t_B$

This type of Euler rotation is called **finite** (or total-separation) Euler rotation. 
A finite rotation gives the total rotation between two isochrones on two corresponding plates relative to the starting position.
It defines the relative position of two plates at the time defined by the isochrones belong.

A rotation ${}^0_B\operatorname{ROT}^t_A$ with the negative Euler angle $-\psi$ or the rotation ${}^t_A\operatorname{ROT}^0_B$ backwards in time rotates the plate A with respect to plate B

${}^0_A\operatorname{ROT}^t_B = -{}^0_B\operatorname{ROT}^t_A = -{}^t_A\operatorname{ROT}^0_B$

In both cases the rotations are relative motions, where in the first case the plate A, and in the second case the plate B, is held fixed.

If the Euler rotations ${}^0_A\operatorname{ROT}^t_B$  for plate B with respect to plate A, and ${}^0_B\operatorname{ROT}^t_C$ for a plate C with respect to plate B, are known, the rotation for the plate C with respect to A ${}^0_A\operatorname{ROT}^t_C$ can be calculated. 
It is the sum of the first two rotations:

${}^0_A\operatorname{ROT}^t_C = {}^0_B\operatorname{ROT}^t_C + {}^0_A\operatorname{ROT}^t_B$

With this method the relative motions between plates can be calculated, if there is no direct interrelation. It is possible to combine several particular reconstructions to a global image.


### Stage poles

Stage poles define the rotation pole of two plates for a time interval of constant rotation between two given times $t_1$ and $t_2$. 
Every stage pole of one plate is associated with a stage pole of the other plate for the same time interval. 
The finite rotation for this time interval leads to coincidence of the two stage poles. 
At the time interval when the stage pole is active there is only one stage pole for both plates. 
This pole is the stage pole and the finite pole simultaneously, but as both plates continue their motions with time in different directions, the reference systems for both plates will separate and a different stage pole for each plate will result. 
The sum of a series of stage poles starting from present day position is the finite rotation

${}^0_A\operatorname{ROT}^{t_n}_B = {}^0_A\operatorname{ROT}^{t_1}_B + {}^{t_1}_A\operatorname{ROT}^{t_2}_B + \ldots + {}^{t_{n-1}}_A\operatorname{ROT}^{t_n}_B$

The stage poles can be calculated if the finite rotations of the time series $t_1 \ldots t_n$ are known. We start with

$-{}^0_A\operatorname{ROT}^{t_1}_B + {}^0_A\operatorname{ROT}^{t_2}_B  = -{}^0_A\operatorname{ROT}^{t_1}_B + {}^{0}_A\operatorname{ROT}^{t_1}_B + {}^{t_1}_A\operatorname{ROT}^{t_1}_B + {}^{t_1}_A\mathrm{ROT}^{t_2}_B  \Rightarrow  {}^{t_1}_A\operatorname{ROT}^{t_2}_B  = -{}^0_A\operatorname{ROT}^{t_1}_B +-{}^0_A\operatorname{ROT}^{t_2}_B$

The calculated stage pole is the rotation pole of plate B with respect to plate A. The stage pole of plate A with respect to plate B is

${}^{t_1}_B\operatorname{ROT}^{t_2}_A = -{}^{0}_B\operatorname{ROT}^{t_1}_A + {}^{0}_B\operatorname{ROT}^{t_2}_A = {}^{0}_A\operatorname{ROT}^{t_1}_B - {}^{0}_A\operatorname{ROT}^{t_2}_B$

Under the assumption, that the rate of motion and the direction of motion does not change between $t_1$ and $t_2$, small circles around this pole show the path of motion for plate A with respect to plate B. 
Therefore it is necessary to know as many finite poles as possible to calculate the stage poles.

### Velocity of motion

The amount of the **angular velocity** $\vec \omega$ can be calculated from the rotation angle $\psi$ in the time interval $[t_1, t_2]$

${}^{t_1}_A\omega^{t_2}_B = \frac{\psi}{t_2-t_1}$

The angular velocity is claculated with `angular_velocity()` giving the time interval $t_1$, $t_2$ and the rotation angle $\psi$:
```{r angular_velocity, include=FALSE}
angular_vel <- function(t1, t2, psi){
  ang.vel <- psi / (t2 - t1)
  return(ang.vel)
}
```

```{r angular_velocity_example}
angular_vel(10, 20, 45)
```


Taking angular velocity into account, the **relative velocity** $\vec v$ of a point on plate B relative to plate A can be calculated

$\vec v = \vec \omega \times R \vec P = \omega \vec E \times R \vec P = \omega R (\vec E \times \vec P)$


The **absolute** value of $\vec v$ (linear velocity) can be calculated from the pole distance $\alpha$ between a point $\vec P$ and the Euler pole $\vec E$

$v = \omega r = \omega R \sin \alpha$

where $r$ is the radius of the small circle of the motion of the point $\vec P$ ($r = R \sin\alpha$).

For example, absolute velocity along a spreading ridge  at the Mid-Atlantic Ridge at the Euler pole (62.4°N, 135.8°E), and at the max distance of the pole:
```{r atlantic1}
r.earth <- 6370 # km 
w <- 0.21 # °/Myr

v0 <- deg2rad(w) * r.earth * sind(0)
v90 <- deg2rad(w) * r.earth * sind(90)

v0
v90
```
The absolute velocity at the Euler pole is 0, whereas at the most distant point is ca. 23 km/Myr (23 mm/yr).

How large is the relative velocity (in °/Myr) between the North-American and Eurasian plate (absolute velocity 1.76 cm/yr) at 66° N and 20°E?
```{r atlantic2}
v <- 1.76*10 # cm/yr (1 cm/yr =  10 mm/yr = 10 km/Myr)
p <- c(66, 20)
ep.atlantic <- c(62.4, 135.8)

alpha <- orthodrome(p, ep.atlantic)
w <- v / (r.earth * sind(alpha)) 
rad2deg(w)
```











### Interpolation of rotation poles

The angular velocity can also be used to calculate the intermediate position of two plates between times of known rotation poles, if a rotation pole for the requested time is not available. 
If the poles at times $t_1$ and $t_2$ are known for two plates A and B and the age $t_x$ of the desired reconstruction lies between $t_1$ and $t_2$, the intermediate rotation angle is ${}^{t_1}_A\psi^{t_x}_B =  (t_2 - t_x) {}^{t_1}_A\omega^{t_2}_B$.

With the assumption, that the stage pole does not change between $t_x$ and $t_2$, the total rotation is

${}^{0}_A\operatorname{ROT}^{t_x}_B = {}^{0}_A\operatorname{ROT}^{t_1}_B + {}^{t_1}_A\operatorname{ROT}^{t_x}_B$ where ${}^{t_1}_A\operatorname{ROT}^{t_x}_B = \operatorname{ROT}[{}^{t_1}_A\vec E^{t_2}_B], (t_2-t_1){}^{t_1}_A\omega^{t_2}_B$.

***

## GPLATES

# Input
 A *.rot file can be read as a tibble using `read.table()` and the following parameters:
```{r rotfile_input}
file <- read.table("//zfs1.hrz.tu-freiberg.de/fak3geo/GEM/Tobias Stephan/GPLATES/Pannotia/Pannotia.rot",
                     header = F, sep="", dec=".") %>% 
  as_tibble() %>% 
  rename(plate=V1, age=V2, lat=V3, lon=V4, angle=V5, fixed=V6, sep=V7, description=V8) %>% # change header 
  arrange(plate, age)
  file
``` 
> The description column does not include additional **white space**! Use "_" to separate words!

The GPLATES rot-file contains the equivalent rotations of each plate relative to a fixed plate.
Each row is a finite rotation of a plate relative to a fixed plate. 
The sum of all finite rotations of the plate and the fixed plate are the total reconstruction pole.

```{r test, include=F}
#filter(df, plate==701| plate==301 | plate==998)

#rotation bis 300 Ma
#rot301 <- euler_rot(euler_pole(90, 0, NA, "polar"), 0) + euler_rot(euler_pole(90, 0, NA, "polar"), 0)
#rot701 <- euler_rot(euler_pole(90, 0, NA, "polar"), 0) + euler_rot(euler_pole(48.2195, 1.7134, NA, "polar"), -62.9240) + euler_rot(euler_pole(32.2616, 5.7767, NA, "polar"), -29.8919)
#euler_from_rot(rot701)
```





## Some tests
Calculation of the Neo-Tethys Euler poles and angles from AFR-EEC rotation between 300 -- 250 Ma:
```{r neo-tethys}
p.neo250 <- c(-9.7038, -61.0314)
p.neo300 <- c(-18.2936, -42.3868)
euler_from_points(p.neo300, p.neo250)
```

Calculation of the Paleo-Tethys Euler poles and angles from AFR-EEC rotation between 370 -- 300 Ma:
```{r paleo-tethys}
p.pt300 <- c(-5.7285, -58.2649)
p.pt370 <- c(-19.8101, -48.4716)
euler_from_points(p.pt370, p.pt300)
```

Calculation of the Paleo-Arctic Euler pole and angle from NAM-EEC rotation between 540 -- 400 Ma:
```{r paleo-arctic_nam}
r <- 6378.14
pa540_1 <- c(11.6195, -137.5540)
pa400_1 <- c(10.5980, -87.4049)
d1 <- 5472.0769
e1 <-euler_from_points(pa540_1, pa400_1)
a11 <- orthodrome(pa540_1, pa400_1)
a12 <- orthodrome_from_distance(d1, r)

pa540_2 <- c(11.5506, -137.6620)
pa400_2 <- c(10.5401, -87.576)
d2 <- 5466.3607
e2 <-euler_from_points(pa540_2, pa400_2)
a21 <- orthodrome(pa540_2, pa400_2)
a22 <- orthodrome_from_distance(d2, r)

a <- mean(a11, a12, a21, a22)
a.sd <- sd(c(a11, a12, a21, a22))
data.frame(Mean=a, Std=a.sd)

e <- euler_pole(mean(e1$euler.pole$Lat, e2$euler.pole$Lat), mean(e1$euler.pole$Lon, e2$euler.pole$Lon), NA, "polar")
bind_rows(e, inverse_euler_pole(e))
```

Calculation of the Paleo-Arctic Euler pole and angle from SIB-EEC rotation between 370 -- 400 Ma:
```{r paleo-arctic_sib}
r <- 6378.14
pa540_1 <- c(4.0692, 178.7217)
pa370_1 <- c(-6.4072, 119.9998)
d1 <- 6629.4839
e1 <-euler_from_points(pa540_1, pa370_1)
a11 <- orthodrome(pa540_1, pa370_1)
a12 <- orthodrome_from_distance(d1, r)

pa540_2 <- c(7.4394, -59.6669)
pa370_2 <- c(12.2984, -120.0005)
d2 <- 6626.7643
e2 <-euler_from_points(pa540_2, pa370_2)
a21 <- orthodrome(pa540_2, pa370_2)
a22 <- orthodrome_from_distance(d2, r)

a <- mean(a11, a12, a21, a22)
a.sd <- sd(c(a11, a12, a21, a22))
data.frame(Mean=a, Std=a.sd)

e <- euler_pole(mean(e1$euler.pole$Lat, e2$euler.pole$Lat), mean(e1$euler.pole$Lon, e2$euler.pole$Lon), NA, "polar")
bind_rows(e, inverse_euler_pole(e))
```


## References

Cox, A., Hart, R.B., 1986. Plate Tectonics: How it Works. Blackwell Scientific Publications, Oxford, 392 pp.

**Greiner, B., 1999, Euler rotations in plate-tectonic reconstructions: Computers & Geosciences, v. 25, p. 209–216, doi: http://dx.doi.org/10.1016/S0098-3004(98)00160-5.**

