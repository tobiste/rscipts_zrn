mds.preparation=function(zrn.data, strat.time, sample.lbls, cut.off, k)
{

#Transponieren der Datenmatrix
MDS_ages=c(); MDS.with.errors=c()
for(i in 1 : max(zrn.data$sample.id) ){
  sample.i=subset(zrn.data, sample.id==i)
  if(length(sample.i$rock)>0){
    #if(sample.i$rock[1]=="sed"){
        sample.name=sample.i$sample[1]
        ages=sample.i$age2 #disc.2
        length(ages)=max
        ages=cbind(ages)
        errors=sample.i$error2 #disc.2
        length(errors)=max
        errors=cbind(errors)
        colnames(ages)=sample.name
        colnames(errors)=sample.name
        MDS_ages=cbind(MDS_ages, ages); MDS.with.errors=cbind(MDS.with.errors,errors)
    #}
  }
}

if(strat.time == "Ref"){sample.lbls="rnd"; plot.title="nMDS of Reference samples with no ages younger than"; filename2="MDS_plots/0-Ref_KDEsummary.pdf"; filename1="MDS_plots/0-Ref_Cluster.pdf"; filename_plotly="0_Ref"}

if(strat.time == "PCC"){plot.title="nMDS of all pre-Ordovician samples with no ages younger than"; filename2="MDS_plots/1-PCC_KDEsummary.pdf"; filename1="MDS_plots/1-PCC_Cluster.pdf"; filename_plotly="1_PCC"}
if(strat.time == "PC"){plot.title="nMDS of all Precambrian samples with no ages younger than";   filename2="MDS_plots/2-PC_KDEsummary.pdf"; filename1="MDS_plots/2-PC_Cluster.pdf"; filename_plotly="2_PC"}
if(strat.time == "Cryog"){plot.title="nMDS of all Cryogenian samples with no ages younger than";  filename2="MDS_plots/3-Cryog_KDEsummary.pdf";  filename1="MDS_plots/3-Cryog_Cluster.pdf"; filename_plotly="3_Cryog"}
if(strat.time == "Edia"){plot.title="nMDS of all Ediacaran samples with no ages younger than";  filename2="MDS_plots/4-Edia_KDEsummary.pdf";  filename2="MDS_plots/4-Edia_Cluster.pdf"; filename_plotly="4_Edia"}
if(strat.time == "Cam"){plot.title="nMDS of all Cambrian samples with no ages younger than";      filename2="MDS_plots/5-Cam_KDEsummary.pdf";      filename1="MDS_plots/5-Cam_Cluster.pdf"; filename_plotly="5_Cam"}
if(strat.time == "EdiaCam"){plot.title="nMDS of all Ediacaran-Cambrian samples with no ages younger than";   filename2="MDS_plots/4-EdiaCam_KDEsummary.pdf";   filename1="MDS_plots/4-EdiaCam_Cluster"; filename_plotly="5_EdiaCam"}
if(strat.time == "L.Ord"){plot.title="nMDS of all Lower Ordovician samples with no ages younger than";  filename2="MDS_plots/6-OrdL_KDEsummary.pdf";  filename1="MDS_plots/6-OrdL_Cluster.pdf"; filename_plotly="6_OrdL"}
if(strat.time == "M.Ord"){plot.title="nMDS of all Middle Ordovician samples with no ages younger than";  filename2="MDS_plots/7-OrdM_KDEsummary.pdf";  filename1="MDS_plots/7-OrdM_Cluster.pdf"; filename_plotly="7_OrdM"}
if(strat.time == "U.Ord"){plot.title="nMDS of all Upper Ordovician samples with no ages younger than";  filename2="MDS_plots/8-OrdU_KDEsummary.pdf";  filename1="MDS_plots/8-OrdU_Cluster.pdf"; filename_plotly="8_OrdU"}
if(strat.time == "Ord"){plot.title="nMDS of all Ordovician samples with no ages younger than"; filename2="MDS_plots/9-Ord_KDEsummary.pdf"; filename1="MDS_plots/9-Ord_Cluster.pdf"; filename_plotly="9_Ord"}
if(strat.time == "Sil"){plot.title="nMDS of all Silurian samples with no ages younger than";    filename2="MDS_plots/10-Sil_KDEsummary.pdf";    filename1="MDS_plots/10-Sil_Cluster.pdf"; filename_plotly="10_Sil"}
if(strat.time == "L.Dev"){plot.title="nMDS of all Lower Devonian samples with no ages younger than";  filename2="MDS_plots/11-DevL_KDEsummary.pdf";  filename1="MDS_plots/11-DevL_Cluster.pdf"; filename_plotly="11_DevL"}
if(strat.time == "M.Dev"){plot.title="nMDS of all Middle Devonian samples with no ages younger than";  filename2="MDS_plots/12-DevM_KDEsummary.pdf";  filename1="MDS_plots/12-DevM_Cluster.pdf"; filename_plotly="12_DevM"}
if(strat.time == "U.Dev"){plot.title="nMDS of all Upper Devonian samples with no ages younger than";  filename2="MDS_plots/13-DevU_KDEsummary.pdf";  filename1="MDS_plots/13-DevU_Cluster.pdf"; filename_plotly="13_DevU"}
if(strat.time == "Dev"){plot.title="nMDS of all Devonian samples with no ages younger than";      filename2="MDS_plots/14-Dev_KDEsummary.pdf";      filename1="MDS_plots/14-Dev_Cluster.pdf"; filename_plotly="14_Dev"}
if(strat.time == "Crb"){plot.title="nMDS of all Carboniferous samples with no ages younger than";   filename2="MDS_plots/15-Crb_KDEsummary.pdf";   filename1="MDS_plots/15-Crb_Cluster.pdf"; filename_plotly="15_Crb"}
if(strat.time == "pvr"){cut.off=0; plot.title="nMDS of all post-Variscan samples"; filename2="MDS_plots/16-pvr_KDEsummary.pdf"; filename1="MDS_plots/16-pvr_Cluster.pdf"; filename_plotly="16_pvr"}
if(strat.time == ">400 Ma"){plot.title="nMDS of all samples older than with no ages younger than";  filename2="MDS_plots/17-all_KDEsummary.pdf";  filename1="MDS_plots/17-all_Cluster.pdf"; filename_plotly="nMDS-Variscides"}
if(strat.time == "all"){plot.title="nMDS of all samples with no ages younger than"; filename2="MDS_plots/18-all_KDEsummary.pdf"; filename1="MDS_plots/18-all_Cluster.pdf"; filename_plotly="18_all"}

if(strat.time == "Aval"){plot.title="nMDS of all Avalonia or Baltica derived samples with no ages younger than"; filename2="MDS_plots/19-Avalonia_KDEsummary.pdf"; filename1="MDS_plots/19-Avalonia_Cluster.pdf"; filename_plotly="19_Avalonia"}
if(strat.time == "CamOrd"){plot.title="nMDS of all Cambro-Ordovician samples with no ages younger than"; filename2="MDS_plots/20-CamOrd_KDEsummary.pdf"; filename1="MDS_plots/20-CamOrd_Cluster.pdf"; filename_plotly="20_CamOrd"}
if(strat.time == "Paleoz1"){plot.title="nMDS of all 541 - 393 Ma samples with no ages younger than";  filename2="MDS_plots/21-Paleoz1_KDEsummary.pdf";  filename1="MDS_plots/21-Paleoz1_Cluster.pdf"; filename_plotly="21_Paleoz1"}
if(strat.time == "Paleoz2"){plot.title="nMDS of all 541 - 299 Ma samples with no ages younger than"; filename2="MDS_plots/22-Paleoz2_KDEsummary.pdf"; filename1="MDS_plots/22-Paleoz2_Cluster.pdf"; filename_plotly="22_Paleou2"}
if(strat.time == "Gondw"){plot.title="nMDS of Gondwana derived samples with no ages younger than";   filename2="MDS_plots/23-Gondw_KDEsummary.pdf";   filename1="MDS_plots/23-Gondw_Cluster.pdf"; filename_plotly="23_Gondwana"}
if(strat.time == "Iberia"){ plot.title="nMDS of Iberian samples with no ages younger than"; filename2="MDS_plots/24-Iberia_KDEsummary.pdf"; filename1="MDS_plots/24-Iberia_Cluster.pdf"; filename_plotly="24_Iberia"}
if(strat.time == "Africa"){plot.title="nMDS of African samples with no ages younger than";   filename2="MDS_plots/25-Africa_KDEsummary.pdf";   filename1="MDS_plots/25-Africa_Cluster.pdf"; filename_plotly="25_Africa"}
if(strat.time == "Appal"){plot.title="nMDS of Appalachian samples with no ages younger than";   filename2="MDS_plots/26-Appal_KDEsummary.pdf";   filename1="MDS_plots/26-Appal_Cluster.pdf"; filename_plotly="26_Appal"}

write.csv(MDS_ages, file="MDS_ages.csv", na="", row.names=FALSE)
#write.csv(MDS.with.errors, file="MDS_errors.csv", na="", row.names=FALSE)


mdsZ2 = read.csv("MDS_ages.csv")
#mdsZ2.errors = read.csv("MDS_errors.csv")
names(mdsZ2)=colnames(MDS_ages) #read.csv ver�ndert strings des headers, hiermit ersetze ich die "neuen" sample_names mit den eigentlichen

write.csv(mdsZ2, file="MDS_ages_noNA.csv", na="", row.names=FALSE)
#write.csv(mdsZ2.errors, file="MDS_errors_noNA.csv", na="", row.names=FALSE)

DZ = read.distributional("MDS_ages_noNA.csv"#, "MDS_errors_noNA.csv"
)

MDS.KS.DS=MDS(DZ, k=3, classical=F) #dissimilarity measure, Kolmogorov-Smirnov statistic, non-metric multidimensional scaling, k-dimensional output
#MDS.SH.DS=MDS(diss(DZ, method="SH")) #Silcombe-Hazelton dissimilarity, non-metric multidimensional scaling 



# Multidimensional scaling
DZages=mdsZ2
mds=data.frame(MDS.X=MDS.KS.DS$points[,1], MDS.Y=MDS.KS.DS$points[,2], MDS.Z=MDS.KS.DS$points[,3], sample=names(DZages), stringsAsFactors = F, row.names = NULL) # f�r qplot

q=unique(data.frame(sample=zrn2$sample, Nr=zrn2$ref.nr, Tectonic.unit=zrn2$unit.abbr, Peaks=zrn2$peaks, Sample.age=zrn2$sample.age, Reference=zrn2$reference, Rock=zrn2$rock, Sample.ID=zrn2$sample.id, Prov=zrn2$ref.sample, Sign=zrn2$signat, Sign2=zrn2$signat2, Long=zrn2$long, Lat=zrn2$lat, stringsAsFactors = F))
mds2=merge(mds, q, all.x=T, by.x="sample", by.y="sample")

n = length(mds2$sample) - length(mds2[grep("ref", mds2$Peaks),]$sample) # anzahl der samples, exkl. der reference-samples

UPb=KDEs(DZ, from=250, to=3600, normalise=F, adaptive = T, bw=30, samebandwidth = TRUE)

if(sample.lbls == "name"){MDSplot= qplot(mds2$MDS.X, mds2$MDS.Y, colour=mds2$Sign2, data=mds2, xlab="Dim1", ylab="Dim2", 
                                   main=paste(plot.title, cut.off, "Ma, S =", round(MDS.KS.DS$stress, digits=3), "(n =", n, ", k >=", k, ")"), 
                                   label=mds2$sample, 
                                   #label=mds2$Tectonic.unit, 
                                   size=I(0.5), alpha=I(1/200))
}

if(sample.lbls == "unit") {MDSplot= qplot(mds2$MDS.X, mds2$MDS.Y, colour=mds2$Sign2, data=mds2, xlab="Dim1", ylab="Dim2", 
               main=paste(plot.title, cut.off, "Ma, S =", round(MDS.KS.DS$stress, digits=3), "(n =", n, ", k >=", k, ")"), 
               label=mds2$Tectonic.unit, 
               size=I(0.5), alpha=I(1/200))
}

if(sample.lbls == "nr") {MDSplot= qplot(mds2$MDS.X, mds2$MDS.Y, colour=mds2$Sign2, data=mds2, xlab="Dim1", ylab="Dim2", 
                                          main=paste(plot.title, cut.off, "Ma, S =", round(MDS.KS.DS$stress, digits=3), "(n =", n, ", k >=", k, ")"),                                           label=mds2$Nr, 
                                          size=I(10), alpha=I(1/200))
}

if(sample.lbls == "sign") {MDSplot= qplot(mds2$MDS.X, mds2$MDS.Y, colour=mds2$Sign2, data=mds2, xlab="Dim1", ylab="Dim2", 
                                        main=paste(plot.title, cut.off, "Ma, S =", round(MDS.KS.DS$stress, digits=3), "(n =", n, ", k >=", k, ")"),                                         label=mds2$Sign, 
                                        size=I(10), alpha=I(1/200))
}

if(sample.lbls == "rnd") {MDSplot= qplot(mds2$MDS.X, mds2$MDS.Y, colour=mds2$Prov, data=mds2, xlab="Dim1", ylab="Dim2", 
                                     main=paste(plot.title, cut.off, "Ma, S =", round(MDS.KS.DS$stress, digits=3), "(n =", n, ", k >=", k, ")"),                                      label=mds2$sample,
                                     size=I(0.5), alpha=I(1/200))
}

if(sample.lbls == "name" | sample.lbls == "unit" | sample.lbls == "sign" | sample.lbls == "nr"){
plot.mds=
  MDSplot +
  geom_text(size=3) +
  xlab("") + ylab("") +
  scale_y_continuous(breaks=NULL) + scale_x_continuous(breaks=NULL) +
  #scale_colour_manual(values = c("darkcyan", "seagreen", "skyblue2", "tan3", "orangered", "goldenrod1", "firebrick4", "magenta1", "darkmagenta", "purple", "black", "lightgray"),
                    #name = "Known age spectra",
                    #breaks = c( "Amazonas", "Avalon", "Baltica", "Cadomian", "Grenvill", "1.8Ga", "2.1Ga", "mPProt", "2.6Ga", "Leonian", "reference", NA),
                    #limits = c( "Amazonas", "Avalon", "Baltica", "Cadomian", "Grenvill", "1.8Ga", "2.1Ga", "mPProt", "2.6Ga", "Leonian", "reference", NA),
                    #labels = c("Amazonian", "Avalonian", "Svecofennian", "Cadomian (650 - 550 Ma)", "Grenvilian (1.0 Ga)", "1.8 Ga", "Eburnean (2.1 Ga)", "Middle Paleoproterozoic", "2.6 Ga", "Leonian ( 3.5 - 3.0 Ga)", "reference", NA),
                    #na.value = "gray") +
  
  scale_colour_manual(values = c("goldenrod1", "slateblue2", "skyblue2", "turquoise2", "violetred", "orangered", "firebrick4", "coral4", "seagreen",  "gray10", "lightgray"),
  name = "Provenance Signature",
  breaks = c( "ANS", "Laurentia", "Baltica", "Baltica-Amazonia", "RC-EG", "SMC", "W.Afr", "W.Afr-Amazonia", "Amazonia", "reference", "unknown"),
  limits = c( "ANS", "Laurentia", "Baltica", "Baltica-Amazonia", "RC-EG", "SMC", "W.Afr", "W.Afr-Amazonia", "Amazonia", "reference", "unknown"),
  labels = c("Arabian-Nubian Shield", "Laurentia", "Baltica", "Baltica-Amazonia", "Rayner Complex - Eastern Ghates", "Sahara Meta-Craton", "West Africa", "West Africa - Amazonia", "Amazonia", "reference", "NA"),
  na.value = "gray") +
  theme_bw() +
  theme(
  panel.background = element_rect(fill = "white", color = "black", size = .5), legend.position = "bottom", legend.text = element_text(size = 6))
}

if(sample.lbls == "rnd") {
  plot.mds=
    MDSplot +
    geom_text(size=3) +
    xlab("") + ylab("") +
    scale_y_continuous(breaks=NULL) + scale_x_continuous(breaks=NULL) +
    theme_bw() +
    theme(
      panel.background = element_rect(fill = "white", color = "black", size = .5), legend.position = "bottom", legend.text = element_text(size = 6))
}


#summary plot with all aKDE plots
  pdf(file=filename2, paper="a4")
  if(length(unique(zrn.data$sample)) < 100) {summaryplot(UPb, ncol=1)}
  if(length(unique(zrn.data$sample)) >=100 & length(unique(zrn.data$sample)) < 200) {summaryplot(UPb, ncol=2)}
  if(length(unique(zrn.data$sample)) >=200 & length(unique(zrn.data$sample)) < 600) {summaryplot(UPb, ncol=3)}
  if(length(unique(zrn.data$sample)) >=600 ) {summaryplot(UPb, ncol=4)}
  dev.off()


# 3-dimensional nMDS
# plot3d(mds2$MDS.X, mds2$MDS.Y, mds2$MDS.Z, xlab="Dim1", ylab="Dim2", zlab="Dim3", col="skyblue3", size=5, main="3d nMDS", sub=paste(plot.title, cut.off, "Ma, S =", round(MDS.KS.DS$stress, digits=3), "(n =", n, ", k >=", k, ")"))
# scatter3D(mds2$MDS.X, mds2$MDS.Y, mds2$MDS.Z, col=gg.col(100), pch=".", type="p", size=8, colvar=as.numeric(mds2$Sign2))

if(strat.time == "Gondw"){
    p=plot_ly(type="scatter3d", x=mds2$MDS.X, y=mds2$MDS.Y, z=mds2$MDS.Z, mode="text+markers", text = mds2$sample, 
            marker = list(color=log(mds2$Sample.age), colorscale=c("darkred", "lemonchiffron2", "royalblue4"), showscale = TRUE) )%>%
      layout(scene = list(xaxis = list(title = 'Dim1'),
                        yaxis = list(title = 'Dim2'),
                        zaxis = list(title = 'Dim3')), 
           title=paste(plot.title, cut.off, "Ma, S =", round(MDS.KS.DS$stress, digits=3), "(n =", n, ", k >=", k, ")"))
}
  
if(strat.time != "Gondw"){  
mds2$Sign2[is.na(mds2$Sign2)]="unknown"

  
p=plot_ly(type="scatter3d", x=-mds2$MDS.X, y=-mds2$MDS.Y, z=-mds2$MDS.Z, mode="text+markers", text = mds2$sample, 
          color=mds2$Sign2, 
          hovertext=paste("Sample", mds2$sample,
                          "\nReference:", mds2$Reference, 
                          "\nAge:", mds2$Sample.age, 
                          "Ma\nUnit:", mds2$Tectonic.unit,
                          "\nLong:", round(mds2$Long, digits=2), 
                          "Lat:", round(mds2$Lat, digits=2))#,
          #colors=c("seagreen", "goldenrod1", "skyblue2", "turquoise2", "slateblue2","violetred", "gray10", "orangered", "lightgray", "firebrick4", "coral4") 
          )%>%
  layout(scene = list(xaxis = list(title = 'Dim1'),
                      yaxis = list(title = 'Dim2'),
                      zaxis = list(title = 'Dim3')), 
         title=paste("nMDS of all samples older than 400 Ma with no ages younger than 700 Ma, S =", round(MDS.KS.DS$stress, digits=3), "(n =", n, ", k >=", k, ")"))
}
# Create a shareable link to your chart
# Set up API credentials: https://plot.ly/r/getting-started
plotly_POST(p, filename="final")

return(plot.mds)
}

#data(Namib)
#mds <- MDS(Namib$DZ,k=3)
#p=plot_ly(type="scatter3d", x=mds$points[,1], y=mds$points[,2], z=mds$points[,3], mode="text+markers", text = row.names(mds$points)); plotly_POST(p, filename="Namib")



#p=plot_ly(type="scatter", x=mds2$MDS.X, y=mds2$MDS.Y, mode="text+markers", text = mds2$sample, 
#          color=mds2$Sign2, hovertext=paste("Reference:", mds2$Reference, "\nAge:", mds2$Sample.age, "Ma\nUnit:", mds2$Tectonic.unit),
#          colors=c("seagreen", "goldenrod1", "skyblue2", "turquoise2", "slateblue2","violetred", "gray10", "orangered", "lightgray", "firebrick4", "coral4") )%>%
#  layout(scene = list(xaxis = list(title = 'Dim1'),
#                      yaxis = list(title = 'Dim2'),
#                      zaxis = list(title = 'Dim3')), 
#         title=paste("test"));p

